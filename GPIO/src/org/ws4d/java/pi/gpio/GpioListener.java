package org.ws4d.java.pi.gpio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

// Die Klasse zur Steuerung von GPIOs auf Raspberry Pi
public class GpioListener {
	// create gpio controller
	private GpioController gpio = null;

	// provision gpio pin #02 as an input pin with its internal pull down resistor enabled
	private GpioPinDigitalInput myButton0 = null;

	// provision gpio pin #02 as an input pin with its internal pull down resistor enabled
	private GpioPinDigitalInput myButton1 = null;
	
	private int pinNumber0 				= -1;
	
	private int pinNumber1				= -1;
	
	private DefaultDevice device = null;
	
	private DefaultClient client = null;
	
	private boolean helloActivated = false;
	
	private boolean probeActivated = false;
	
	private long lastRequestTime0 = 0;
	private long lastRequestTime1 = 0;
	
	private long resetTimer = 0;
	
	static GpioListener instance; 
	
	private ArrayList<ButtonListener> listeners = new ArrayList<ButtonListener>();
		
	// Suppress default constructor for noninstantiability
    private GpioListener() {}
    
    public static synchronized GpioListener getInstance() {
    	if (GpioListener.instance == null) {
    		GpioListener.instance = new GpioListener();
    	}
    	return GpioListener.instance;
    }
    
    // Controller Getter
    public GpioController getGpioController()
	{
		return gpio;
	}
	
    // Button 0 Getter
	public GpioPinDigitalInput getButton0()
	{
		return myButton0;
	}
	
	// Button 1 Getter
	public GpioPinDigitalInput getButton1()
	{
		return myButton1;
	}
	
	// Button 0 zu einem Pin binden 
	public void bindButton0(int pinNumber)
	{
		if (gpio == null)
			gpio = GpioFactory.getInstance();
		
		if (pinNumber0 != pinNumber)
		{
			pinNumber0 = pinNumber;
			if (myButton0 != null)
				gpio.unprovisionPin(myButton0);

			myButton0 = gpio.provisionDigitalInputPin(getPin(pinNumber), PinPullResistance.PULL_DOWN);
		}
		
		if (device != null)
		{
			instance.activateInterrupt();
		}
	}
	
	// Button 1 zu einem Pin binden
	public void bindButton1(int pinNumber)
	{
		if (gpio == null)
			gpio = GpioFactory.getInstance();
		
		if (pinNumber1 != pinNumber)
		{
			pinNumber1 = pinNumber;
			if (myButton1 != null)
				gpio.unprovisionPin(myButton1);

			myButton1 = gpio.provisionDigitalInputPin(getPin(pinNumber), PinPullResistance.PULL_DOWN);
		}
		
		if (device != null)
		{
			instance.activateInterrupt();
		}
	}
	
	// Spezielle Pin-Nummerierung
	private static Pin getPin(int pinNumber)
	{
		switch(pinNumber){
		case 0: return RaspiPin.GPIO_00;
		case 1: return RaspiPin.GPIO_01;
		case 2: return RaspiPin.GPIO_02;
		case 3: return RaspiPin.GPIO_03;
		case 4: return RaspiPin.GPIO_04;
		case 5: return RaspiPin.GPIO_05;
		case 6: return RaspiPin.GPIO_06;
		case 7: return RaspiPin.GPIO_07;
		case 8: return RaspiPin.GPIO_08;
		case 9: return RaspiPin.GPIO_09;
		case 10: return RaspiPin.GPIO_10;
		case 11: return RaspiPin.GPIO_11;
		case 12: return RaspiPin.GPIO_12;
		case 13: return RaspiPin.GPIO_13;
		case 14: return RaspiPin.GPIO_14;
		case 15: return RaspiPin.GPIO_15;
		case 16: return RaspiPin.GPIO_16;
		case 17: return RaspiPin.GPIO_17;
		case 18: return RaspiPin.GPIO_18;
		case 19: return RaspiPin.GPIO_19;
		case 20: return RaspiPin.GPIO_20;
		default: Log.warn("Pin is out of bounds");
		}
		return null;		
	}
	
	// �berpr�fen, ob gew�hlte Tasten gedr�ckt sind
	public boolean areButtonsHigh()
	{
//		System.out.println("Button 1 is " + myButton0.isHigh() + "; Button 2 is " + myButton1.isHigh());
		if (myButton0 != null && myButton1 != null)
		{
			if (myButton0.isHigh() && myButton1.isHigh())
			{
				return true;
			}
			else return false;
		}
		if (myButton0 != null)
		{
			if (myButton0.isHigh())
			{
				return true;
			}
			else return false;
		}
		
		if (myButton1 != null)
		{
			if (myButton1.isHigh())
			{
				return true;
			}
			else return false;
		}
		
		return true;		
	}
	
	// �berpr�fen, ob gew�hlte Tasten innerhalb einer Periode gedr�ckt werden
	public boolean areButtonsHighDelayed(int delay)
	{
		client.requestReceived();
		int i = 0;
		int delayInternal = delay * 10;
		while (i < delayInternal)
		{		
			if (areButtonsHigh())
			{
				System.out.println("High delayed accepted");
				client.requestAccepted();
				return true;
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
		client.requestRejected();
		return false;		
	}
	
	// Device f�r ein Interupt registrieren
	public void registerDevice(DefaultDevice device)
	{
		this.device = device;
		activateInterrupt();
	}
	
	// Client f�r ein Interrupt registrieren
	public void registerClient(DefaultClient client)
	{
		this.client = client;
		activateInterrupt();
	}
	
	// Hello Interrupt aktivieren
	public void activateHello(boolean activate)
	{
		helloActivated = activate;
		//activateInterrupt();
	}
	
	// �berpr�fen, ob Hello Interrupt aktiviert ist
	public boolean isHelloActivated()
	{
		return helloActivated;
	}
	
	// Probe Interrupt aktivieren
	public void activateProbe(boolean activate)
	{
		probeActivated = activate;
		//activateInterrupt();
	}
	
	// �berpr�fen, ob Probe Interrupt aktiviert ist  
	public boolean isProbeActivated()
	{
		return probeActivated;
	}
	
	// Device Getter
	public DefaultDevice getDevice()
	{
		return device;		
	}
	
	// Client Getter
	public DefaultClient getClient()
	{
		return client;
	}
	
	// Tasten-Interrupts aktivieren
	private void activateInterrupt()
	{
		if (myButton0 != null)
		{
			// create and register gpio pin listener
	        myButton0.addListener(new GpioPinListenerDigital()
	        {
	            @Override
	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event)
	            {	      
	            	// Tastenentprellung
	            	long currentTime = Calendar.getInstance().getTimeInMillis();
	            	if ((currentTime - lastRequestTime0) > 300)
	            	{
	            		lastRequestTime0 = currentTime;
	            		
	            		if (myButton0.isHigh())
	            		{
	            			buttonPressed(0);
	            		}	            		
	            		
	            		if (myButton0.isLow() && Calendar.getInstance().getTimeInMillis() - resetTimer > 3000)
	            		{
	            			client.resetPairing();
	            		}

	            		resetTimer = Long.MAX_VALUE;
	            		
	            		// display pin state on console
//	            		System.out.println(" --> GPIO PIN STATE CHANGE 1: " + event.getPin() + " = " + event.getState());
	            		if (areButtonsHigh())
	            		{
//	            			client.resetPairing();
	            			resetTimer = Calendar.getInstance().getTimeInMillis();
	            			Log.info("Checking if Hello and Probe activated");
	            			if (GpioListener.getInstance().getDevice() != null && GpioListener.getInstance().isHelloActivated())
	            			{
	            				Log.info("Sending hello");
	            				device.sendHello();
	            			}

	            			if (GpioListener.getInstance().getClient() != null && GpioListener.getInstance().isProbeActivated())
	            			{
	            				Log.info("Sending probe");
//	            				client.resetPairing();
	            				client.searchKnownDevices();
	            			}
	            		}
	            	}
	            }
	            
	        });
		}
		
		if (myButton1 != null)
		{
			// create and register gpio pin listener
	        myButton1.addListener(new GpioPinListenerDigital()
	        {
	            @Override
	            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event)
	            {
	            	// Tastenentprellung
	            	long currentTime = Calendar.getInstance().getTimeInMillis();
	            	if ((currentTime - lastRequestTime1) > 300)
	            	{
	            		lastRequestTime1 = currentTime;
	            		
	            		if (myButton1.isHigh())
	            		{
	            			buttonPressed(1);
	            		}
	            		
	            		if (myButton1.isLow() && Calendar.getInstance().getTimeInMillis() - resetTimer > 3000)
	            		{
	            			client.resetPairing();
	            		}

	            		resetTimer = Long.MAX_VALUE;
	            		
	            		// display pin state on console
//	            		System.out.println(" --> GPIO PIN STATE CHANGE 2: " + event.getPin() + " = " + event.getState());
	            		if (areButtonsHigh())
	            		{
//	            			client.resetPairing();
	            			resetTimer = Calendar.getInstance().getTimeInMillis();
	            			Log.info("Checking if Hello and Probe activated");
	            			if (GpioListener.getInstance().getDevice() != null && GpioListener.getInstance().isHelloActivated())
	            			{
	            				Log.info("Sending hello");
	            				device.sendHello();
	            			}

	            			if (GpioListener.getInstance().getClient() != null && GpioListener.getInstance().isProbeActivated())
	            			{
	            				Log.info("Sending probe");
//	            				client.resetPairing();
	            				client.searchKnownDevices();
	            			}
	            		}
	            	}
	            }
	            
	        });
		}
	} 
	
	// Listener setzen
	public void addListener(ButtonListener toAdd) {
        listeners.add(toAdd);
    }
	
	// Button Event Notifier
	public void buttonPressed(int pinNumber) {
		
		final int pin = pinNumber;
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				// Notify everybody that may be interested.
				Log.info("Button pressed: " + pin);
				for (ButtonListener bl : listeners)
					bl.buttonPressed(pin);
			}
		});
		t.start();

	}
}
