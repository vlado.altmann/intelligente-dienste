package org.ws4d.java.pi.gpio;

// Button Interface
public interface ButtonListener {
	public void buttonPressed(int pinNumber);
}
