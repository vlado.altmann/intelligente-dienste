package com.pi.airconditioner.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

import com.pi.airconditioner.device.AirCondGPIOListener;
import com.pi.airconditioner.device.GetTemperatureOperation;
import com.pi.airconditioner.gui.MainWindow;


// Dieser Client wird zur Kommunikation mit anderen Services verwendet
public class AirCondClient extends DefaultClient {
	public final static String	NAMESPACE	= "http://www.demo.com/bbsr";

	public final static String	SENSOR_DEVICE		= "Sensor";
	
	public final static String	SENSOR_SERVICE		= "SensorInterface";
	
	public final static String	WINDOW_DEVICE		= "SmartWindow";

	public final static String	WINDOW_SERVICE		= "SmartWindowInterface";
	
	boolean bound							= false;
	
	private Service sensorService;
	
	private Service windowService;
	
	private Timer timer;
	
	private javax.swing.Timer timer2;
	
	private MainWindow gui					= null;
	
	private boolean requested				= false;
	
	private GetTemperatureOperation getOp;
	
	private int outdoorTemperature = 0;
	
	// Dient der zyklischen abfrage der Temperaturwert von einem Sensor.
	private class Task extends TimerTask
	{
		private AirCondClient client;
		
		public Task(AirCondClient airCondClient)
		{
			client = airCondClient;
		}
		
		@Override
		public void run()
		{
			client.updateTemperature();
		}
	}
		
	// Callback Funktion. Sie wird aufgerufen wenn ein Device gefunden wurde.
	@Override
	public synchronized void deviceFound(DeviceReference devRef, SearchParameter search) {
		QNameSet types = devRef.getDiscoveryData().getTypes();
		org.ws4d.java.structures.Iterator i = types.iterator();
//		System.out.println("Found Device: " + devRef.toString());
		
		// Ein Sensor-Service und ein Window-Service k�nnen angebunden werden.
		while (i.hasNext() && (sensorService == null || windowService == null))
		{
			QName qname = (QName) i.next();
			if ((qname.getLocalPart().equals(SENSOR_DEVICE) && sensorService == null) || (qname.getLocalPart().equals(WINDOW_DEVICE) && windowService == null))
			{
				System.out.println("Device: " + qname.getLocalPart());
				Log.info("Connecting...");
				if (gui != null)
				{
					gui.setStatus("connecting...");
				}
				try {
					Device device = devRef.getDevice();
//					QNameSet serviceTypes = new QNameSet();
//					serviceTypes.add(new QName(SENSOR_SERVICE, NAMESPACE));
//					serviceTypes.add(new QName(WINDOW_SERVICE, NAMESPACE));
					org.ws4d.java.structures.Iterator serviceIterator = device.getServiceReferences(null, SecurityKey.EMPTY_KEY);
					DefaultServiceReference serviceRef = (DefaultServiceReference) serviceIterator.next();
					
					Log.info("Chosing device...");
					
					// Sensor gefunden
					if (qname.getLocalPart().equals(SENSOR_DEVICE))
					{
						Log.info("Sensor");
						sensorService = serviceRef.getService();
						//					System.out.println("ServiceId: " + service.toString());
						Log.info("Connected");
						if (gui != null)
						{
							gui.setStatus("connected");
						}

						bound = true;

						Task task = new Task(this); 
						timer = new Timer();
						timer.schedule(task, 5000, 60000);

//						updateTemperature();
					}
					
					// Window gefunden
					else
					{			
						Log.info("Window");
						windowService = serviceRef.getService();
						//					System.out.println("ServiceId: " + service.toString());
						Log.info("Connected");
						if (gui != null)
						{
							gui.setStatus("connected");
						}

						bound = true;
						
					}

				} catch (CommunicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (AuthorizationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	// Suche ist nach einem Timeout abgeschlossen
	@Override
	public void finishedSearching(boolean entityFound, SearchParameter search) {
		System.out.println("entityFound: " + entityFound + "; requested: " + requested + "; bound: " + bound);
		// Wenn nichts gefunden
		if (!requested && gui != null && !bound)
		{
			Log.info("Not connected");
			gui.setStatus("not connected");
		}
		// Wenn was gefunden
		else if ((requested || bound) && gui != null)
		{
			Log.info("Connected");
			gui.setStatus("connected");
		}
		requested = false;
	}
	
	// Hello-Nachricht ist eingegangen
	@Override
	public void helloReceived(HelloData helloData)
	{
		Log.info("Hello received");
//		gui.setStatus("connection requested...");
		// Warte auf Tasten
		if (GpioListener.getInstance().areButtonsHighDelayed(DispatchingProperties.getInstance().getResponseWaitTime() / 1000))
		{
//			resetPairing();
			Log.info("Hello accepted");
			Log.info("Connecting...");
			if (gui != null)
			{
				gui.setStatus("connecting...");
			}
			DeviceReference devRef = getDeviceReference(helloData);
			deviceFound(devRef, null);
		}
		// Tasten nicht gedr�ckt
		else 
		{
			Log.info("Hello rejected");
//			gui.setStatus("not connected");
		}
	}
	
	// Suche nach Ger�ten starten
	@Override
	public void searchKnownDevices()
	{
		Log.info("Searching for devices...");
		if (!requested && gui != null)
		{
			Log.info("Searching...");
			gui.setStatus("searching...");
		}
		SearchManager.searchDevice(null, this, null);
		
//		search = new SearchParameter();
//		qnameSet = new QNameSet();
//		qnameSet.add(new QName(SENSOR_DEVICE, NAMESPACE));
////		qnameSet.add(new QName(WINDOW_DEVICE, NAMESPACE));
//		search.setDeviceTypes(qnameSet);
//		SearchManager.searchDevice(search, this, null);
	}
	
	// Bekannte Services f�r Hello Listening registrieren
	public void registerKnownHello()
	{
		SearchParameter search = new SearchParameter();
		QNameSet qnameSet = new QNameSet();
		qnameSet.add(new QName(SENSOR_DEVICE, NAMESPACE));
		search.setDeviceTypes(qnameSet);
		this.registerHelloListening(search);
	}
	
	// Ger�te-Pairing wird aufgehoben, Einstellungen werden zur�ckgesetzt
	@Override
	public void resetPairing()
	{
		Log.info("Resetting pairing");
		bound = false;
		if (gui != null)
		{
			gui.cleanOutdoorTemp();
			gui.setStatus("reseted");
		}
		if (timer != null)
			timer.cancel();
		if (timer2 != null)
			timer2.stop();
		
		sensorService = null;
		windowService = null;
	}
	
	// Temperatur vom Sensor aktualisieren
	public void updateTemperature()
	{
		// we are looking for an Operation by its name
		Operation op = sensorService.getOperation(null, "TemperatureAction", null, null);
		ParameterValue input = op.createInputValue();
		ParameterValue result;

		try {
			// Nachricht schicken
			result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			System.out.println("Response from the TemperatureAction: " + result.toString());
			if (gui != null && bound)
			{
				// Antwort verarbeiten
				Double temp = Double.parseDouble(result.toString());
				outdoorTemperature = temp.intValue();
				gui.setOutdoorTemp(outdoorTemperature, null);
				
				checkTemperature();
			}
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Window �ffnen
	private void openWindow()
	{
		Log.info("Opening Window");
		switchWindowState("open");
	}
	
	// Window schlie�en
	private void closeWindow()
	{
		Log.info("Closing Window");
		switchWindowState("close");
	}
	
	// Window-Zustand umkehren -> Wenn auf geht zu
	private void switchWindowState(String state)
	{
		// we are looking for an Operation by its name
		Operation op = windowService.getOperation(null, "Switch", null, null);
		ParameterValue input = op.createInputValue();
		ParameterValue result;

		ParameterValueManagement.setString(input, "switch", state);

		try {
			result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			System.out.println("Response from the Window: " + result.toString());
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Temperatur mit dem Sollwert vergleichen und handeln
	public void checkTemperature()
	{
		// Wenn beide Services verbunden kann Window auf-/zugemacht werden
		if (sensorService != null && windowService != null)
		{
			if (Math.abs(outdoorTemperature - getOp.getTemperature()) < 2)
			{
				openWindow();				
			}
			else
			{
				closeWindow();
			}
		}
		
		// Wenn nur Sensor verbunden kann nur Ventilator ein/-ausgeschaltet werden
		if (sensorService != null)
		{
			if (outdoorTemperature - getOp.getTemperature() > 2)
			{
				AirCondGPIOListener.getInstance().turnOn();
			}
			else
			{
				AirCondGPIOListener.getInstance().turnOff();
			}
		}
	}
	
	// GUI Setter
	public void setGui(MainWindow gui)
	{
		this.gui = gui;
	}
	
	// Operation Setter
	public void setTemperatureOperation(GetTemperatureOperation op)
	{
		getOp = op;
	}
	
	// Ein Anfrage (Probe) ist eingegangen
	@Override
	public void requestReceived()
	{
		requested = true;
		Log.info("Connection requested...");
		if (gui != null)
		{
			gui.setStatus("connection requested...");
		}
	}
	
	// Anfrage angenommen (Tasten gedr�ckt)
	@Override
	public void requestAccepted()
	{
//		requested = false;
		bound = true;
		Log.info("Request accepted");
		if (gui != null)
		{
			gui.setStatus("connected");
		}
	}
	
	// Anfrage abgelehnt (Taste nicht gedr�ckt)
	@Override
	public void requestRejected()
	{
		requested = false;
		Log.info("New connection rejected");
		if (gui != null && !bound)
		{
			Log.info("Not connected...");
			gui.setStatus("not connected");
		}
		else if (gui != null)
		{
			Log.info("connected...");
			gui.setStatus("connected");
		}
	}
	
//	public static void main(String[] args)
//	{
//		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
//		DPWSProperties properties = DPWSProperties.getInstance();
//		properties.addSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION2006));
//
//		JMEDSFramework.start(args);
//
//		AirCondClient client = new AirCondClient();
//		client.searchKnownDevices();
//
//	}

}
