package com.pi.airconditioner.device;

import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;

import com.pi.airconditioner.client.AirCondClient;
import com.pi.airconditioner.gui.MainWindow;

/**
 * Implementierung des Service
 */
public class AirCondService extends DefaultService {

	public final static URI	SERVICE_ID	= new URI(AirCondDevice.NAMESPACE + "/AirCondService");
	public final static String SERVICE = "AirCondInterface";
	
	SetTemperatureOperation setTemp;
	GetTemperatureOperation getTemp;

	/**
	 * Standard Constructor
	 */
	public AirCondService() {
		super();

		this.setServiceId(SERVICE_ID);

		// add Operations to the service		
		getTemp = new GetTemperatureOperation();
		addOperation(getTemp);

		setTemp = new SetTemperatureOperation();
		addOperation(setTemp);
		
		setTemp.setTemperatureOperation(getTemp);
		
		IndoorTemperatureEvent temperatureEvent = new IndoorTemperatureEvent();
		addEventSource(temperatureEvent);
		setTemp.setTemperatureEvent(temperatureEvent);

	}
	
	// GUI Setter
	public void setGui(MainWindow gui)
	{
		setTemp.setGui(gui);
	}
	
	// Operation Setter
	public GetTemperatureOperation temperatureOperation()
	{
		return getTemp;
	}
	
	// Client Setter
	public void setClient(AirCondClient client)
	{
		setTemp.setClient(client);
	}

}
