package com.pi.airconditioner.device;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.pi.gpio.ButtonListener;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

import com.pi.airconditioner.client.AirCondClient;
import com.pi.airconditioner.gui.MainWindow;

// Operation zum Setzen der gewŁnschten Temperatur
public class SetTemperatureOperation extends Operation implements ButtonListener {

	private GetTemperatureOperation getOperation;
	private MainWindow gui;
	private IndoorTemperatureEvent temperatureEvent;
	private AirCondClient client;

	public SetTemperatureOperation() {
		super("SetDesiredTemperature", new QName(AirCondService.SERVICE, AirCondDevice.NAMESPACE));

		// create inner elements for complex type
		Element value = new Element(new QName("value", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_INT);
		Element unit = new Element(new QName("unit", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_STRING);
		Element time = new Element(new QName("timeStamp", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_TIME);
		time.setMinOccurs(0);

		// create complex type and add inner elements
		ComplexType temperatureType = new ComplexType(new QName("TemperatureType", AirCondDevice.NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
		temperatureType.addElement(value);
		temperatureType.addElement(unit);
		temperatureType.addElement(time);

		// create element of type TemperatureType
		Element temperatureElement = new Element(new QName("temperature", AirCondDevice.NAMESPACE), temperatureType);

		// set the input of the operation
		setInput(temperatureElement);

		// create reply element
		Element reply = new Element(new QName("status", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set this element as output
		setOutput(reply);
		
		GpioListener.getInstance().addListener(this);
	}

	// Operation wird angefragt
	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		// get string value from input
		String value = ParameterValueManagement.getString(parameterValue, "value");
		String unit = ParameterValueManagement.getString(parameterValue, "unit").toUpperCase();
		System.out.println("value " + value + "; unit " + unit);

		// create output and set value
		ParameterValue result = createOutputValue();		
		
		try {
			int intValue = Integer.parseInt(value);

			// GewŁnschte Einheit
			if (unit.equals("C") || unit.equals("F") || unit.equals("K"))
			{
				getOperation.setTemperature(intValue);
				getOperation.setUnit(unit);
				
				gui.setIndoorTemp(intValue, unit);
				client.checkTemperature();

				ParameterValueManagement.setString(result, "status", "OK");
				
				temperatureEvent.fireTemperatureEvent(getOperation.getTemperature(), getOperation.getUnit());
			}
			// Flasche Einheit gesetzt
			else
			{
				ParameterValueManagement.setString(result, "status", "Wrong Unit");
			}
		}
		catch (NumberFormatException e){
			ParameterValueManagement.setString(result, "status", "Wrong Number Format");
		}

		return result;
	}
	
	// Operation Setter
	public void setTemperatureOperation(GetTemperatureOperation op)
	{
		getOperation = op;
	}
	
	// GUI Setter
	public void setGui(MainWindow gui)
	{
		this.gui = gui;
	}
	
	// Steuerung der Temperatur mittels Tasten
	@Override
	public void buttonPressed(int pinNumber)
	{
		int temp = getOperation.getTemperature();
		if (pinNumber == 0)
		{
			temp--;
			getOperation.setTemperature(temp);			
		}
		if (pinNumber == 1)
		{
			temp++;
			getOperation.setTemperature(temp);
		}
		gui.setIndoorTemp(getOperation.getTemperature(), getOperation.getUnit());
		client.checkTemperature();
//		gui.setStatus("Updating button " + pinNumber + ";temp:" + temp);
		
		temperatureEvent.fireTemperatureEvent(getOperation.getTemperature(), getOperation.getUnit());
		
	}
	
	// Event Setter
	public void setTemperatureEvent(IndoorTemperatureEvent event)
	{
		temperatureEvent = event;
	}
	
	// Client Setter
	public void setClient(AirCondClient client)
	{
		this.client = client;
	}
}
