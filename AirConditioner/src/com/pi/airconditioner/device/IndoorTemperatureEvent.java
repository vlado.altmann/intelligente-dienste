package com.pi.airconditioner.device;

import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

// Event zur Aktualisierung der eingestellten Temperatur auf anderen Ger�ten
public class IndoorTemperatureEvent extends DefaultEventSource {
	
	private int eventCounter = 1;

	public IndoorTemperatureEvent() {
		super("IndoorTemperatureEvent", new QName(AirCondService.SERVICE, AirCondDevice.NAMESPACE));
		
		// create inner elements for complex type
		Element value = new Element(new QName("value", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_INT);
		Element unit = new Element(new QName("unit", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_STRING);
		Element time = new Element(new QName("timeStamp", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_TIME);
		time.setMinOccurs(0);

		// create complex type and add inner elements
		ComplexType temperatureType = new ComplexType(new QName("TemperatureType", AirCondDevice.NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
		temperatureType.addElement(value);
		temperatureType.addElement(unit);
		temperatureType.addElement(time);

		// create element of type TemperatureType
		Element temperatureElement = new Element(new QName("temperature", AirCondDevice.NAMESPACE), temperatureType);

		// set the input of the operation
		//				setInput(null);

		// set this element as output
		setOutput(temperatureElement);
	}
	
	// Event ausl�sen
	public void fireTemperatureEvent(int value, String unit)
	{
		// create output and set value
		ParameterValue result = createOutputValue();
		ParameterValueManagement.setString(result, "value", "" + value);
		ParameterValueManagement.setString(result, "unit", unit);
		ParameterValueManagement.setString(result, "timeStamp", "2012-01-09T09:00:00");
		
		fire(result, eventCounter++, CredentialInfo.EMPTY_CREDENTIAL_INFO);
	}

}
