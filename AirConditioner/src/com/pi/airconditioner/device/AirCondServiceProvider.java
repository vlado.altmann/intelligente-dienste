package com.pi.airconditioner.device;

import java.awt.EventQueue;
import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

import com.pi.airconditioner.client.AirCondClient;
import com.pi.airconditioner.gui.MainWindow;

public class AirCondServiceProvider {

	/**
	 * Main Funktion
	 * @param args
	 */
	public static void main(String[] args) {

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
		properties.setDiscoveryButton0(0);
		properties.setDiscoveryButton1(3);
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
		//properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);
		// Tasten-Timeout
		DispatchingProperties.getInstance().setResponseWaitTime(10000);
		
		// mandatory: Starting the DPWS Framework.
		JMEDSFramework.start(args);
		
		// Ein Ger�t erzeigen
		AirCondDevice device = new AirCondDevice();
		final AirCondClient client = new AirCondClient();

		// Ein Service erzeugen
		final AirCondService service = new AirCondService();

		// Service dem Ger�t zuweisen
		device.addService(service);

		// Device starten
		try {
			device.start();
			client.registerKnownHello();
			GpioListener.getInstance().registerDevice(device);
			GpioListener.getInstance().registerClient(client);
			GpioListener.getInstance().activateHello(true);
			GpioListener.getInstance().activateProbe(true);
//			Thread.sleep(5000);
//			client.resetPairing();
//			client.searchKnownDevices();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//GUI Start
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					client.setGui(frame);
					client.setTemperatureOperation(service.temperatureOperation());
					service.setGui(frame);
					service.setClient(client);
//					String test = "29.69000000009";
//					frame.setOutdoorTemp(Double.parseDouble(test));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
}
