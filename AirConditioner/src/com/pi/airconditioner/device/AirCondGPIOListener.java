package com.pi.airconditioner.device;

import org.ws4d.java.pi.gpio.GpioListener;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

// Die Klasse zur Ansteuerung des Ventilator �ber GPIO
public class AirCondGPIOListener {
	// create gpio controller instance
	final GpioController gpio = GpioFactory.getInstance();
	// provision gpio pins #04 as an output pin and make sure is is set to LOW at startup
	GpioPinDigitalOutput ventilator = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07,   // PIN NUMBER
	                                                           "Ventilator",           // PIN FRIENDLY NAME (optional)
	                                                           PinState.LOW);      // PIN STARTUP STATE (optional)
	
	static AirCondGPIOListener instance; 
	
	// Suppress default constructor for noninstantiability
    private AirCondGPIOListener() {}
    
    public static synchronized AirCondGPIOListener getInstance() {
    	if (AirCondGPIOListener.instance == null) {
    		AirCondGPIOListener.instance = new AirCondGPIOListener();
    	}
    	return AirCondGPIOListener.instance;
    }
	
    // Einschalten
	public void turnOn()
	{
		ventilator.high();
		System.out.println("Turning On");
	}

	// Ausschalten
	public void turnOff()
	{
		ventilator.low();
		System.out.println("Turning Off");
	}
	
	// Kurzes Einschalten
	public void pulse(int millis)
	{
		ventilator.pulse(millis);
	}
}
