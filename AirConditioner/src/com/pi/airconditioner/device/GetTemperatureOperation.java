package com.pi.airconditioner.device;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

// Operation zur Abfrage der aktuellen Temperatur
public class GetTemperatureOperation extends Operation {

	private int temperature = 24;
	private String temperatureUnit = "C"; 

	public GetTemperatureOperation() {
		super("GetCurrentTemperature", new QName(AirCondService.SERVICE, AirCondDevice.NAMESPACE));

		// create inner elements for complex type
		Element value = new Element(new QName("value", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_INT);
		Element unit = new Element(new QName("unit", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_STRING);
		Element time = new Element(new QName("timeStamp", AirCondDevice.NAMESPACE), SchemaUtil.TYPE_TIME);
		time.setMinOccurs(0);

		// create complex type and add inner elements
		ComplexType temperatureType = new ComplexType(new QName("TemperatureType", AirCondDevice.NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
		temperatureType.addElement(value);
		temperatureType.addElement(unit);
		temperatureType.addElement(time);

		// create element of type TemperatureType
		Element temperatureElement = new Element(new QName("temperature", AirCondDevice.NAMESPACE), temperatureType);

		// set the input of the operation
//		setInput(null);

		// set this element as output
		setOutput(temperatureElement);
	}

	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		// create output and set value
		ParameterValue result = createOutputValue();
		ParameterValueManagement.setString(result, "value", "" + temperature);
		ParameterValueManagement.setString(result, "unit", temperatureUnit);
		ParameterValueManagement.setString(result, "timeStamp", "2012-01-09T09:00:00");

		return result;
	}
	
	// Temperaturwert Setter
	public void setTemperature(int value)
	{
		temperature = value;
	}
	
	// Einheit Setter
	public void setUnit(String unit)
	{
		temperatureUnit = unit;
	}
	
	// Temperatur und Einheit Setter
	public void setTemperature(int value, String unit)
	{
		temperature = value;
		temperatureUnit = unit;
	}
	
	// Temperatur Getter
	public int getTemperature()
	{
		return temperature;
	}
	
	// Einheit Getter
	public String getUnit()
	{
		return temperatureUnit;
	}
}
