/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import java.io.IOException;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import org.ws4d.java.communication.tcp.IConnection;
import org.ws4d.java.communication.tcp.IConnectionFactory;
import org.ws4d.java.logging.Log;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;

/**
 * SunSPOTCLDCConnectionFactory replaces CLDCConnectionFactory of the original ws4d sources.
 * It implements a customized protocol to establish a peer-to-peer (stream) connection between
 * two parties.
 * 
 * modified by Christin Groba
 *
 */
public class SunSPOTCLDCConnectionFactory implements IConnectionFactory {
	private final String PEERCONNECTIONREQUEST = "ConnectMe";
	
	/**
	 * Default constructor.
	 */
	public SunSPOTCLDCConnectionFactory() {}

	/**
	 * Uses the <code>Connector</code> class to open a CLDC Connection.
	 * @throws IOException 
	 */
	public synchronized IConnection openConnection(String host, int port) throws IOException  {
		SunSPOTCLDCConnection streamcon;
		
		RadiostreamConnection con = openRadioStreamConnection(host, port);
		streamcon = new SunSPOTCLDCConnection(con, null, null);	
		return streamcon;
	}
	
	private synchronized RadiostreamConnection openRadioStreamConnection(String address, int port) throws IOException {
		int peerport = -1;
		RadiogramConnection requestconn, replyconn;
		
		// open a new datagram connection where the peerPort will be received
		// it has to be a new connection and not the send connection because otherwise this method would not receive the reply but the HTTPSever
		replyconn = (RadiogramConnection) Connector.open("radiogram://");
		int replyport = replyconn.getLocalPort();
		Log.debug("Open radiogram listener for stream port on port: "+replyport);
		
		// open a radiogram connection to request the peer connection request
		requestconn = (RadiogramConnection) Connector.open("radiogram://" + address + ":" + port);
		Datagram dg = requestconn.newDatagram(requestconn.getMaximumLength());
		dg.reset();
		dg.writeUTF(PEERCONNECTIONREQUEST+":"+replyport);
		requestconn.send(dg);
		requestconn.close();
		
		// listen on the replyconn for the peerport on which to create the stream connection
		Datagram dgreply = replyconn.newDatagram(replyconn.getMaximumLength());
		dgreply.reset();
		replyconn.receive(dgreply);
		peerport = dgreply.readInt();
		Log.debug("Stream port received: "+peerport);			
		replyconn.close();

		// create stream connection
		RadiostreamConnection radioStreamConnection = (RadiostreamConnection) Connector.open("radiostream://" + address + ":" + peerport);

		return radioStreamConnection;
	}
}
