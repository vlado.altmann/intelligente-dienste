/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.ws4d.java.communication.tcp.IConnection;

import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;

/**
 * SunSPOTCLDCConnection replaces CLDCConnection of the original ws4d sources.
 * The OutputStream and InputStream are already created with creating the stream connection.
 * This is necessary because the system can allocate the next available port only if an 
 * OutputStream or an InputStream has been created on the connection. OpenInputStream()
 * and openOutputStream() check first whether the existing is/os are null. If this is the case
 * a new stream is created.
 * 
 * modified by Christin Groba
 * 
 */
public class SunSPOTCLDCConnection implements IConnection {
	/** The underlying CLDC Connection object. */
	private RadiostreamConnection	streamConnection;
	private OutputStream 			os;
	private InputStream 			is;

	/**
	 * Default constructor. Initializes the object.
	 * 
	 * @param streamConnection the CLDC connection.
	 */
	public SunSPOTCLDCConnection(RadiostreamConnection streamConnection, OutputStream os, InputStream is) {
		this.streamConnection = streamConnection;
		this.os = os;
		this.is = is;
	}

	/**
	 * Opens an <code>InputStream</code> on the StreamConnection.
	 * 
	 * @return an InputStream.
	 */
	public InputStream openInputStream() throws IOException {
		if(is==null) return streamConnection.openInputStream();
		
		return is;
	}

	/**
	 * Opens an <code>OutputStream</code> on the StreamConnection.
	 * 
	 * @return an OutputStream.
	 */
	public java.io.OutputStream openOutputStream() throws IOException {
		if(os==null) return streamConnection.openOutputStream();
		
		return os;
	}

	/**
	 * Closes the connection.
	 */
	public void close() throws IOException {
		streamConnection.close();
	}

}
