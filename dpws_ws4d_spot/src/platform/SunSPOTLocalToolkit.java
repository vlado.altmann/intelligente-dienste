/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import java.io.IOException;
import java.io.PrintStream;

import org.ws4d.java.communication.tcp.IConnection;
import org.ws4d.java.communication.tcp.IConnectionFactory;
import org.ws4d.java.communication.tcp.IServerSocket;
import org.ws4d.java.communication.udp.AbstractUDPServer;
import org.ws4d.java.communication.udp.IDatagramConnection;
import org.ws4d.java.logging.Log;
import org.ws4d.java.platform.ByteVector;
import org.ws4d.java.platform.IByteVector;
import org.ws4d.java.platform.IPAddress;
import org.ws4d.java.platform.NotSupportedException;
import org.ws4d.java.platform.PlatformToolkit;
import org.ws4d.java.util.Properties;
import org.ws4d.java.util.concurrency.AbstractLock;
import org.ws4d.java.util.concurrency.ILockedHashtable;
import org.ws4d.java.util.concurrency.ILockedVector;
import org.ws4d.java.util.concurrency.LockedHashtable;
import org.ws4d.java.util.concurrency.LockedVector;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.util.IEEEAddress;

/**
 * This is the local toolkit for Sun SPOTs. It replaces LocalToolkit of the original
 * ws4d sources. It calls other SPOT-specific classes within the platform package. It
 * sets the device IP address to base station IP address as specified in 
 * device.SunSpotApplication. Only then the base station is able to forward all unicast 
 * messages from the DPWS explorer to free-range SPOT.
 * 
 * modified by Christin Groba
 *
 */
public class SunSPOTLocalToolkit extends PlatformToolkit {
	private IConnectionFactory	connectionFactory	= null;
	private int localUnicastPort 					= 0; 

	/**
	 * Returns a new ByteVector().
	 * 
	 * @return a ByteVector.
	 */
	public IByteVector createByteVector() throws IOException {
		return new ByteVector();
	}

	/**
	 * Returns a new ByteVector().
	 * 
	 * @return a ByteVector.
	 */
	public IByteVector createByteVector(int incrementSize) throws IOException {
		return new ByteVector();
	}

	/**
	 * Not Supported. Throws a NotSupportedException.
	 * 
	 * @return a ByteVector.
	 */
	public IByteVector createByteVector(String fileNameWithPath, int incrementSize) throws IOException {
		throw new NotSupportedException("platform_cldc doesn't support the method LocalToolkit.createByteVector(String,int).");
	}

	/**
	 * creates a Server Socket implementation based on the DPWS platform.
	 * 
	 * @return An IServerSocket implementation.
	 */
	public IServerSocket createServerSocket() {
		return new SunSPOTServerSocket();
	}
	
	public IServerSocket createSSLServerSocket() {
		Log.error("Can not create SSLSocket!");
		System.exit(0);
		return null;
	}
	
	public IDatagramConnection createMulticastDatagramConnection(String param) {
		SunSPOTDatagramConnectionImpl impl = null;
		impl = createDatagramConnection(param);
		
		return impl;
	}
	
	public IDatagramConnection createUnicastDatagramConnection(String param) {
		SunSPOTDatagramConnectionImpl impl = null;
		impl = createDatagramConnection(param);
		if(impl!=null) localUnicastPort = impl.getLocalPort();
		
		return impl;
	}

	private SunSPOTDatagramConnectionImpl createDatagramConnection(String param) {
		SunSPOTDatagramConnectionImpl impl = null;
		try {
			impl = new SunSPOTDatagramConnectionImpl(param);
		} catch (IOException e) {
			Log.printStackTrace(e);
		}
		return impl;
	}

	/**
	 * Opens a connection to the specified host/port.
	 * 
	 * @param host the host address.
	 * @param port the port.
	 * @return An IConnection object or null, if an error has occured.
	 */
	public IConnection openConnection(String host, int port) throws IOException {
		if (connectionFactory == null) connectionFactory = new SunSPOTCLDCConnectionFactory();
		return connectionFactory.openConnection(host, port);
	}
	
	public IConnection openSSLConnection(String host, int port) throws IOException {
		Log.error("Can not create SSLSocket!");
		System.exit(0);
		return null;
	}

	/**
	 * returns a description of the toolkit.
	 */
	public String getDescription() {
		return "Sun SPOT (CLDC) Local Toolkit";
	}

	public Thread createThread(Runnable target, String name, boolean daemon) {
		Thread t = new Thread(target);
		return t;
	}

	public ILockedHashtable createLockedHashtable() {
		return new LockedHashtable();
	}

	public ILockedHashtable createLockedHashtable(AbstractLock lockObject) {
		return new LockedHashtable(lockObject);
	}

	public ILockedVector createLockedVector() {
		return new LockedVector();
	}

	public ILockedVector createLockedVector(AbstractLock lockObject) {
		return new LockedVector(lockObject);
	}

	public void printStackTrace(Exception e, PrintStream ps) {
		Log.warn("This platform does not support other PrintStream. Using default output.");
		e.printStackTrace();
	}
	
	/**
	 * Creates a new instance of a local IUDPServer-implementing class.
	 * 
	 * @return a new instance of a local IUDPServer-implementing class.
	 */
	public AbstractUDPServer getUDPServer() {
		final String serverUDPSE = "org.ws4d.java.communication.udp.UDPServerSE";
		final String serverUDPCLDC = "platform.SunSPOTUDPServer";

		if (udpserver != null) {
			return udpserver;
		}
		
		// this is hard wired setting PROP_DPWS_MCAST_MODE does not have an effect
		int mode = 0;

		try {
			Class udpServerClass = null;
			String className = null;

			switch (mode) {
				case USE_INTERNAL_MULTICAST:
				case USE_PLATTFORM_MULTICAST:
					className = serverUDPCLDC;
					break;
				case USE_J2SE_MULTICAST:
					className = serverUDPSE;
					break;
				default:
					Log.error("Could not specify mode for the UPD Server.");
					return null;
			}

			Log.debug("Loading UDP Server implementation: " + className);
			udpServerClass = Class.forName(className);

			AbstractUDPServer udpServer = (AbstractUDPServer) udpServerClass.newInstance();
			return udpServer;
		} catch (ClassNotFoundException e) {
			Log.printStackTrace(e);
			Log.error("ClassNotFoundException! Reason: Configuration entry fault");
			System.exit(0);
		} catch (InstantiationException e) {
			Log.printStackTrace(e);
			Log.error("InstantiationException! Reason: Configuration entry fault");
			System.exit(0);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			Log.printStackTrace(e);
		}
		return null;
	}

	public int getLocalUnicastPort() {
		return localUnicastPort;
	}
	
	/**
	 * Returns the IP address of the local machine. Sun SPOTs do not yet support IP stack.
	 * Therefore we use the device address here.
	 * 
	 * @return String local IP address.
	 */
	public IPAddress getLocalIP() {		
		if (deviceAddress != null) {
			return deviceAddress;
		}	
		
		// first check if global properties set it
		String ipAddress = Properties.getInstance().getGlobalProperty(Properties.PROP_DEVICE_IP_ADDRESS);
		if(ipAddress!=null){
			deviceAddress = new IPAddress(ipAddress);
		}else{
			// set device IEEE address as ip address
			long spotAddr = Spot.getInstance().getRadioPolicyManager().getIEEEAddress();
			deviceAddress = new IPAddress(IEEEAddress.toDottedHex(spotAddr));
		}

		Log.info("Using property IP address " + deviceAddress.get());

		return deviceAddress;
	}

}
