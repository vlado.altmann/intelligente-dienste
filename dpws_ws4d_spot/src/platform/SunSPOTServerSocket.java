/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import java.io.IOException;
import java.util.Random;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import org.ws4d.java.communication.tcp.IConnection;
import org.ws4d.java.communication.tcp.IServerSocket;
import org.ws4d.java.logging.Log;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.io.j2me.radiostream.RadioInputStream;
import com.sun.spot.io.j2me.radiostream.RadioOutputStream;
import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;
import com.sun.squawk.util.StringTokenizer;

/**
 * SunSPOTServerSocket replaces CLDCServerSocket of the original ws4d sources.
 * It creates a server socket. When a client sends a connection request, a stream
 * connection (peer-to-peer) is established between the server and the client.
 * 
 * modified by Christin Groba
 */
public class SunSPOTServerSocket implements IServerSocket {

	/**
	 * The number of tries of finding a random port to listen on before
	 * resigning.
	 */
	protected static final int			PORT_RETRIES				= 3;

	private RadiogramConnection			server						= null;
	
	private final String 				PEERCONNECTIONREQUEST 		= "ConnectMe";

	/**
	 * Default constructor.
	 */
	public SunSPOTServerSocket() {}

	/**
	 * The method waits until a new client connects and returns an IConnection
	 * object.
	 * 
	 * @return a Connection to the client.
	 */
	public IConnection accept() throws IOException {
		return acceptAndOpen();
	}
	
	/**
	 * Implement specific protocol to establish a stream connection between server and client.
	 * It must be a RadioStreamConnection because later there will be input and output streams
	 * opened on it. RadioStreamConnection is a StreamConnection
	 * 
	 * @return
	 */
	
	private SunSPOTCLDCConnection acceptAndOpen() throws IOException{
		Datagram dg = null;
		Datagram dgreply = null;
		RadiostreamConnection streamConnection = null;
		int streamPort = -1;
		
		// wait until something is received from client
		dg = server.newDatagram(server.getMaximumLength());
        dg.reset();
        server.receive(dg);      
        String request = dg.readUTF();
        
        // split request into request type and reply port
        StringTokenizer tok = new StringTokenizer(request, ":");
        int i = 0;
		String requesttpe="";
		String replyport="";
		String token;
        while (tok.hasMoreTokens()) {
			token =  tok.nextToken();
			switch(i){
			case 0: requesttpe = token; break;
			case 1: replyport = token; break;
			default: break;
			}
            i++;
		} 		
        
		// proceed only if a peer connection request was received
        if(!requesttpe.equals(PEERCONNECTIONREQUEST)){
        	return null;
        }
        Log.debug("Received peer connection request. Reply should go to: "+dg.getAddress()+":"+replyport);
        
		// create stream connection and let system allocate port
        // generate random stream port number
        streamPort = generatePort();        
        streamConnection = (RadiostreamConnection)Connector.open("radiostream://"+dg.getAddress()+":"+streamPort);
        RadioInputStream is = (RadioInputStream)streamConnection.openInputStream();
        RadioOutputStream os = (RadioOutputStream) streamConnection.openOutputStream();
        
        Log.debug("Genereated port for stream connection: "+streamPort);
		
        // open the reply radiogram connection
        RadiogramConnection replyconn = (RadiogramConnection) Connector.open("radiogram://" + dg.getAddress() + ":" + replyport);
        
        // send generated port to client
		dgreply = replyconn.newDatagram(replyconn.getMaximumLength());
		dgreply.reset();
		dgreply.writeInt(streamPort);
		replyconn.send(dgreply);
		
		// close replyconn
		replyconn.close();
		
		return new SunSPOTCLDCConnection(streamConnection, os, is);
	}
	
	/**
	 * Generates a random port number between 40 and 254. This leaves some space between 32-40 to
	 * allocate ports for the temporary datagram connections. 255 is the maximum port number allowed
	 * for SPOTs and is already reserved for the HTTP port.
	 * 
	 * @return
	 */
	private int generatePort(){
		int port = -1;
		
		Random random = new Random();
		// generate a number between 0(inclusive) and 215(exclusive) because 254-40=214
		int r = random.nextInt(215);
		port = 40 +r;
		
		return port;
	}
	
	/**
	 * Closes the underlying server socket.
	 */
	public void close() throws IOException {
		server.close();
	}

	/**
	 * The open method opens a listening socket on the specified port number. If
	 * the port number is -1, a system generated port is used.
	 * 
	 * @param port the port number. Use -1 to generate a port number.
	 * @return the used port. If -1 is returned, a error occured.
	 */
	public int open(int port) {
		if (port != -1) {		
			try {
				server = (RadiogramConnection) Connector.open("radiogram://:" + port);
				Log.debug("HTTP server connection opened on radiogram://:" + port);
			} catch (Exception e) {
				Log.error("Unable to open server socket on port " + port);
				//Log.printStackTrace(e);
				Log.error("ERROR: " + e.getClass() + " - " + e.getMessage());
				return -1;
			}
		} else {
			int currentTry = 0;
			while (currentTry < PORT_RETRIES) {
				//int potentialPort = getRandomPortNumber();
				try {
					server = (RadiogramConnection) Connector.open("radiogram://:");
					port = server.getLocalPort();
					break;
				} catch (Exception e) {
					Log.error("Unable to open server socket on system generated port " + port);
					//Log.printStackTrace(e);
					Log.error("ERROR: " + e.getClass() + " - " + e.getMessage());
					
				}
				currentTry++;
			}
			if (currentTry == PORT_RETRIES) return -1;
		}
		return port;
	}
}
