/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.io.Connector;

import org.ws4d.java.communication.udp.Datagram;
import org.ws4d.java.communication.udp.IDatagramConnection;
import org.ws4d.java.logging.Log;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ISwitch;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import com.sun.spot.util.Utils;

/**
 * SunSPOTDatagramConnectionImpl replaces DatagramConnectionImpl of the original ws4d sources.
 * It uses a RadiogramConnection instead of DatagramConnection. A fragmentation protocol is 
 * added to handle Resolve/Probe and ResolveMatch/ProbeMatch messages that exceed the size of a 
 * radiogram.
 * 
 * Packet fragmentation with Continuation bit: 
 * when sending datagrams last byte of each message is either 1 or 0
 * 1: receiver has to wait on further datagrams
 * 0: receiver has received the last datagram to reconstruct the entire message.
 * 
 * modified by Christin Groba
 *
 */
public class SunSPOTDatagramConnectionImpl implements IDatagramConnection {

	private RadiogramConnection	connection		= null;
	int numberOfMessages = 0;
	
	
	/**
	 * Creates a DatagramConnection to the given host.
	 * 
	 * @param con The host to connect to.
	 * 
	 * @throws IOException
	 */
	public SunSPOTDatagramConnectionImpl(String con) throws IOException {
		connection = (RadiogramConnection) Connector.open(con);
	}

	public void close() throws IOException {
		connection.close();
	}

	public int getMaximumLength() throws IOException {
		return connection.getMaximumLength();
	}

	public int getNominalLength() throws IOException {
		return connection.getNominalLength();
	}

	public Datagram receive() throws IOException {
		javax.microedition.io.Datagram dgram = connection.newDatagram(connection.getMaximumLength());
		
		Vector msgVector = new Vector();
		int msgSize = 0;
		byte[] message = null;
		
		// receive several fragmented packets if necessary
		while(true){
			dgram.reset();
			try {
				connection.receive(dgram);
				byte[] b = new byte[dgram.getLength()];
				dgram.readFully(b);
				// add to msgVector - continuation bit is still included
				msgVector.addElement(b);
				Log.debug("Datagram fragment received: "+new String(b));
				// msgSize is without continuation bit
				msgSize = msgSize + (b.length-1);
				// check continuation bit
				int cb = b[(b.length-1)];
				if(cb==0){
					// create complete array
					message = joinMessageFragments(msgVector, msgSize);
					break;
				}
				Log.debug("Wait for next datagram fragment");
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
		}			
		
		Datagram result = new Datagram(dgram.getAddress(), message);
		return result;
	}

	public void send(Datagram datagram) throws IOException {
		javax.microedition.io.Datagram dgram = connection.newDatagram(connection.getMaximumLength());	
		
		// keep within datagram size
		int max = connection.getMaximumLength();
		int off = 0;
		int len = max-1;
		int fragments;
		int counter = 0;
		//########################		
		ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();  // Led ansteuerung
		/**leds[6].setRGB(000, 100, 000);	// Nachricht senden - leds[6] leuchtet gruen 
		leds[6].setOn();
		Utils.sleep(1000);  // f�r eine sekunde
		leds[6].setOff();
		**/
		String message = new String(datagram.getData()); // zugriff auf die Daten erhalten
		
		/**
		 * Da es hier keine String.contains() funktion zu geben scheint behelfe ich mir mit String.indexOf()
		 * wenn ein String str1 nicht teil eines Strings str2 ist wird int -1 zur�ckgegeben
		 */
		int index = message.indexOf("ProbeMatches");
		
		if(index == -1)	// wenn "ProbeMatches" nicht teil der Nachricht ist, ist index = -1
		    {		// die Nachricht kann einfach gesendet werden und die Funktion wird verlassen
			// calculate number of fragments;
			// leds[5].setRGB(100, 000, 000);	    // led[5] auf blau stellen
			// leds[5].setOn();			    // led[5] an	
	    
			fragments = datagram.getData().length/(max-1);
			if((datagram.getData().length % (max-1))!=0) fragments++; //round up

			Log.debug("Forward multicast data in fragments: "+fragments);

			// send message in several fragments if necessary
			for(counter=0; counter < fragments; counter++){
				dgram.reset();
				if(counter==fragments-1){
					//last fragments
					dgram.write(datagram.getData(), off, datagram.getData().length-off);
					dgram.write(0);
				}else{
					dgram.write(datagram.getData(), off, len);
					dgram.write(1);
				}
				connection.send(dgram);
				off = off +len;
			    }
			
		    for(int i=0;i<7;i++)    // alle m�glicherweise noch eingeschalteten Lampen aus
		    {leds[i].setOff();}
		    }
		/**
		* Hier geht es weiter falls "ProbeMatches" gefunden wurde
		*/
		else
		{
		    int countdown = 40;
		    //leds[6].setRGB(000, 000, 100);  // ProbeMatches soll gesendet werden:
		    //leds[6].setOn();		    // Leds[6] leuchtet blau
		    //leds[6].setOff();
		    
		    ISwitch sw1 = EDemoBoard.getInstance().getSwitches()[EDemoBoard.SW1];
		    //ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();

		    for(int i=0;i<3;i++)    // Countdown initialisieren 3 Lampen an
		    {
			leds[i].setRGB(000, 100, 000);
			leds[i].setOn();
		    }
		    boolean sw1Pushed = false;
		    /**
		     * Countdown, alle 10 Z�hler geht eine Lampe aus
		     * wenn Countdown abgelaufen oder Knopf1 gedr�ckt
		     * wird die schleife verlassen
		     */
		    while(countdown!=0)
		    {
			leds[6].setRGB(0, 0, 100);	
			leds[6].setOn();
			
			if(leds[countdown/10].isOn())
			    {leds[countdown/10].setOff();}
			countdown--;
			if(sw1.isClosed())
			{sw1Pushed = true;break;}
			Utils.sleep(300);   //150
		    }
//		    
//		    for(int i=0;i<7;i++)    // alle m�glicherweise noch eingeschalteten Lampen aus
//		    {leds[i].setOff();}
		    
		    if(sw1Pushed == false)
		    {return;}
		    
		    if(countdown == 0)
		    {return;}
		    
		    if(sw1Pushed == true && countdown != 0)	// wenn der Knopf in der Zeit gedr�ckt wurde			
		    {						// wird gesendet, ansonsten nicht
			// leds[5].setRGB(000, 000, 100);
			// leds[5].setOn();
			
			// calculate number of fragments;
			fragments = datagram.getData().length/(max-1);
			if((datagram.getData().length % (max-1))!=0) {fragments++;} //round up

			Log.debug("Forward multicast data in fragments: "+fragments);

			// send message in several fragments if necessary
			for(counter=0; counter < fragments; counter++){
				dgram.reset();
				if(counter==fragments-1){
					//last fragments
					dgram.write(datagram.getData(), off, datagram.getData().length-off);
					dgram.write(0);
				}else{
					dgram.write(datagram.getData(), off, len);
					dgram.write(1);
				}
				connection.send(dgram);
				off = off +len;
			}
		    Utils.sleep(250);
		    for(int i=0;i<7;i++)    // alle m�glicherweise noch eingeschalteten Lampen aus
		    {leds[i].setOff();}
		    }
		}
	}
	//########################
	
	public int getLocalPort(){
		return connection.getLocalPort();
	}
	
	private byte[] joinMessageFragments(Vector parts, int size){
		byte[] message = new byte[size];
		int destPos = 0;
		
		for (Enumeration el = parts.elements(); el.hasMoreElements();) {
			byte[] part = (byte[]) el.nextElement();
			// copy without continuation bit
			System.arraycopy(part, 0, message, destPos, (part.length-1));
			destPos = destPos + (part.length -1);
		}
		return message;
	}

}
