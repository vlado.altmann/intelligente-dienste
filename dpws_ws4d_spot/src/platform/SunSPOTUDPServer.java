/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package platform;

import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import com.sun.spot.util.Utils;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import org.ws4d.java.communication.udp.AbstractDatagramOutputStream;
import org.ws4d.java.communication.udp.AbstractUDPServer;
import org.ws4d.java.communication.udp.Datagram;
import org.ws4d.java.communication.udp.DatagramListener;
import org.ws4d.java.communication.udp.DatagramOutputStream;
import org.ws4d.java.communication.udp.IDatagramConnection;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.io.OpenStreamsTable;
import org.ws4d.java.logging.Log;
import org.ws4d.java.platform.PlatformToolkit;
import org.ws4d.java.util.Properties;
import org.ws4d.java.util.StringUtil;
import org.ws4d.java.util.concurrency.ReadWriteLock;


/**
 * SunSPOTUDPServer replaces UDPServer of the original ws4d sources. It opens a radiogram 
 * server connection and squawk allocates a port. The localUnicastPort is set to system's
 * allocated port. 	Multicast is not supported therefore it is realized via broadcast.
 * 
 * modified by Christin Groba
 *
 */
public class SunSPOTUDPServer extends AbstractUDPServer {

	/**
	 * windows: you have to adjust the ASYNC_BUFFER_SIZE in
	 * kvm/VmExtra/h/async.h if you need more than 3000 bytes per packet
	 */

	/**
	 * The list of listeners that are informed whenever a new multicast UDP
	 * packet is received. The elements are of type
	 * <code>DatagramListener</code>.
	 */
	private Vector				mcastListeners				= new Vector();

	/**
	 * The list of listeners that are informed whenever a new unicast UDP packet
	 * is received. The elements are of type <code>DatagramListener</code>.
	 */
	private Vector				ucastListeners				= new Vector();

	/**
	 * A reference to the thread the receiver runs in.
	 */
	private Thread				mcastthread;

	/**
	 * A reference to the thread the receiver runs in.
	 */
	private Thread				ucastthread;

	/**
	 * The specific datagram connection used for the actual receiving of
	 * packets.
	 */
	private IDatagramConnection	ucastConnection;

	/**
	 * The specific datagram connection used for the actual sending
	 * multicast packets.
	 */
	private IDatagramConnection	mcastSenderConnection;
	
	/**
	 * The specific datagram connection used for the actual receiving of
	 * multicast packets.
	 */
	private IDatagramConnection	mcastReceiverConnection;

	/**
	 * The local unicast port we are listening for incoming packets.
	 */
	private int					localUnicastPort			= 0;


	/**
	 * Reader writer lock for safer multithreading.
	 */
	private final ReadWriteLock	lock						= new ReadWriteLock();

	private static int			connectionMultiCastCounter	= 0;

	private static int			connectionUniCastCounter	= 0;
	
	/**
	 * The broadcast address which should be used instead of multi-casting.
	 * (configured using <code>-BroadcastAddress</code> property in Properties class)
	 */
	private String broadcastAddress;
	
	/**
	 * The hash table maps unicast receiver address to the connection that was created for the client 
	 * to send unicast messages to the receiver.
	 */
	private Hashtable ucastSenderConnections = new Hashtable();

	/**
	 * Default constructor.
	 */
	public SunSPOTUDPServer() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#startReceivers()
	 */
	public void startReceivers() {
		Log.debug("Starting UDP Receivers...");
		try {
			SunSPOTLocalToolkit localToolkit = (SunSPOTLocalToolkit) PlatformToolkit.getToolkit(); // this should be a CLDC LocalToolkit
			
			//create unicast connection
			String con = "radiogram://"; // this is a server listening for incomming msg
			ucastConnection = localToolkit.createUnicastDatagramConnection(con);
			localUnicastPort =  localToolkit.getLocalUnicastPort();
			Log.debug("Unicast server: " + con + ":" + localUnicastPort);			
			Log.info("UDP datagram server socket opened @ port " + localUnicastPort);
			
			// create multicast connection
			// check if we should use broadcasting instead of multicasting
			broadcastAddress = Properties.getInstance().getGlobalProperty(Properties.PROP_BROADCAST_ADDRESS);
			if (broadcastAddress == null) {
				con = DPWSConstants.DPWS_MCAST_ADDRESS;
				Log.debug("Multicast: " + con);
				mcastReceiverConnection = localToolkit.createMulticastDatagramConnection(con);
			} else {			
				// create connection to send multicast message (i.e. via broadcast)
				con = "radiogram://"+broadcastAddress;
				mcastSenderConnection = localToolkit.createMulticastDatagramConnection(con);
				Log.debug("Multicast sender connection: " + con);
				
				// crate connection to receive multicast messages (i.e. via broadcast)
				String broadcastport = StringUtil.split(broadcastAddress, ':')[1];
				con = "radiogram://:" + broadcastport;
				mcastReceiverConnection = localToolkit.createMulticastDatagramConnection(con);
				Log.debug("Multicast receiver connection: " + con);			
			}
		} catch (Exception e) {
			Log.printStackTrace(e);
		}
		setRunning(true);
		mcastthread = PlatformToolkit.getToolkit().createThread(new MulticastReceiver(), "DPWSStack: UDP Multicast Server CLDC", false);
		mcastthread.start();
		ucastthread = PlatformToolkit.getToolkit().createThread(new UnicastReceiver(), "DPWSStack: UDP Unicast Server CLDC", false);
		ucastthread.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#stopReceivers()
	 */
	public void stopReceivers() {
		Log.debug("Stopping UDP Receivers...");
		super.stopReceivers();

		try {
			ucastConnection.close();
			mcastReceiverConnection.close();
			mcastSenderConnection.close();
		} catch (IOException e) {
			Log.printStackTrace(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#addMulticastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void addMulticastListener(DatagramListener mcastlistener) {
		lock.lockWrite();
		try {
			if (mcastlistener != null && !mcastListeners.contains(mcastlistener)) mcastListeners.addElement(mcastlistener);
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#removeMulticastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void removeMulticastListener(DatagramListener mcastlistener) {
		lock.lockWrite();
		try {
			if (mcastlistener != null) mcastListeners.removeElement(mcastlistener);
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#addUnicastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void addUnicastListener(DatagramListener ucastlistener) {
		lock.lockWrite();
		try {
			if (ucastlistener != null && !ucastListeners.contains(ucastlistener)) ucastListeners.addElement(ucastlistener);
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#removeUnicastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void removeUnicastListener(DatagramListener ucastlistener) {
		Log.error(this + " removeUnicastListener");
		lock.lockWrite();
		try {
			if (ucastlistener != null) ucastListeners.removeElement(ucastlistener);
		} finally {
			lock.unlock();
			Log.error(this + " removeUnicastListener");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#addAnycastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void addAnycastListener(DatagramListener acastlistener) {
		lock.lockWrite();
		try {
			addUnicastListener(acastlistener);
			addMulticastListener(acastlistener);
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#removeAnycastListener(edu.udo.cs.sirena.communication.udp.DatagramListener)
	 */
	public void removeAnycastListener(DatagramListener acastlistener) {
		lock.lockWrite();
		try {
			removeUnicastListener(acastlistener);
			removeMulticastListener(acastlistener);
		} finally {
			lock.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#sendDatagram(java.lang.String,
	 *      byte[])
	 */
	public void sendDatagram(String receiverAddrs, byte[] data) throws IOException {
	      
	    ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();
		leds[0].setRGB(000, 000, 200);
		leds[0].setOn();
		Utils.sleep(1000);
		leds[0].setOff();
		
	    if (ucastConnection == null) throw new IOException("Unicast connection is not available");
	    Datagram dgram = new Datagram(receiverAddrs, data);
	    ucastConnection.send(dgram);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#sendMulticastDatagram(byte[])
	 */
	public void sendMulticastDatagram(byte[] data) throws IOException {		
		if (broadcastAddress == null)
			sendDatagram(DPWSConstants.DPWS_MCAST_ADDRESS, data);
		else
			sendDatagram(broadcastAddress, data);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#getOutputStream(java.lang.String)
	 */
	public AbstractDatagramOutputStream getOutputStream(String receiverAddress) {
		//// TODO: check this datagram test
		//if (receiverAddress == null || !receiverAddress.startsWith("datagram://")) return null;
		//return new DatagramOutputStream(receiverAddress, ucastConnection);
		
		if (receiverAddress == null) return null;
		
		// check if an unicast send connection to receiverAddress was already created
		IDatagramConnection ucastSenderConnection = (IDatagramConnection)ucastSenderConnections.get(receiverAddress);
		if(ucastSenderConnection==null){
			String con = "radiogram://"+receiverAddress+":32"; // this is a client sending msg
			ucastSenderConnection = ((SunSPOTLocalToolkit) PlatformToolkit.getToolkit()).createUnicastDatagramConnection(con);
			ucastSenderConnections.put(receiverAddress, ucastSenderConnection);
		}
		
		return new DatagramOutputStream(receiverAddress+":32", ucastSenderConnection);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.udo.cs.sirena.communication.udp.IUDPServer#getMulticastOutputStream()
	 */
	public AbstractDatagramOutputStream getMulticastOutputStream() {
		if (broadcastAddress == null)
			return new DatagramOutputStream(DPWSConstants.DPWS_MCAST_ADDRESS, ucastConnection);
		else
			return new DatagramOutputStream(broadcastAddress, mcastSenderConnection);
	}

	/**
	 * This class implements the receiver thread for multicast datagrams.
	 */
	class MulticastReceiver implements Runnable {

		/**
		 * Open a datagram socket and start listening. Each received datagram is
		 * forwarded to all registered listeners.
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			try {
				Datagram data = null;
				while (isRunning()) {
					//TODO: here we need a listener for broadcast msg "radiogram://:82"
					data = mcastReceiverConnection.receive();
					new NotifyThread(mcastListeners, data);
					connectionMultiCastCounter++;
					Thread t = PlatformToolkit.getToolkit().createThread(new NotifyThread(mcastListeners, data), "DPWSStack: UDP Multicast Listener " + connectionMultiCastCounter, false);
					if (data != null) {
						t.start();
					}
				}
			} catch (IOException e) {
				if (isRunning()) {
					Log.error("UDP Server i/o error: " + e.getMessage());
				} else {
					Log.debug("Stopped Multicast Receiver for closed UDP Server.");
				}
			}
		}

	}

	/**
	 * This class implements the receiver thread for unicast datagrams.
	 */
	class UnicastReceiver implements Runnable {

		/**
		 * Each received datagram is forwarded to all registered listeners.
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			try {
				Datagram data = null;
				while (isRunning()) {
					data = ucastConnection.receive();
					connectionUniCastCounter++;
					Thread t = PlatformToolkit.getToolkit().createThread(new NotifyThread(ucastListeners, data), "DPWSStack: UDP Unicast Listener " + connectionUniCastCounter, false);
					if (data != null) {
						t.start();
					}
				}
			} catch (IOException e) {
				if (isRunning()) {
					Log.error("UDP Server i/o error: " + e.getMessage());
				} else {
					Log.debug("Stopped Unicast Receiver for closed UDP Server.");
				}
			}
		}
	}

	/**
	 * This class notifies all listeners.
	 */
	class NotifyThread implements Runnable {

		private Vector		listeners;

		private Datagram	data;

		public NotifyThread(Vector listeners, Datagram data) {
			this.listeners = listeners;
			this.data = data;
		}

		public void run() {
			org.ws4d.java.communication.udp.Datagram datagram = new org.ws4d.java.communication.udp.Datagram(data.getAddress(), data.getData());
			lock.lockRead();
			try {
				for (int i = 0; i < listeners.size(); i++)
					((DatagramListener) listeners.elementAt(i)).receiveDatagram(datagram);
			} finally {
				lock.unlock();
			}
			OpenStreamsTable.closeStreamsOfThread();
		}
	}
}
