/*
 * Copyright (c) 2006, 2007 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package device;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;


import org.ws4d.java.logging.Log;
import org.ws4d.java.platform.PlatformToolkit;
import org.ws4d.java.util.Properties;

import platform.SunSPOTLocalToolkit;

//import org.ws4d.java.util.Properties;

import ws4dtest.HelloWorldDevice;
import ws4dtest.HelloWorldService;
//import ws4dtest.HelloWorldService;

import com.sun.spot.peripheral.Spot;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ISwitch;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import com.sun.spot.util.BootloaderListener;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;

/**
 * SunSpotApplication starts sets all properties required to use the ws4d library.
 * It starts the service hosted by the SPOT. If switch 1 on the SPOT is pressed
 * the device terminates.
 *  
 * modified by Christin Groba
 *
 */
public class SunSpotApplication extends MIDlet {

    private ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();
    
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("SPOT as DPWS Device");
        new BootloaderListener().start();   // monitor the USB (if connected) and recognize commands from host

            long ourAddr = Spot.getInstance().getRadioPolicyManager().getIEEEAddress();
            System.out.println("Our radio address = " + IEEEAddress.toDottedHex(ourAddr));
        
            // set debugging level
            Properties.getInstance().setGlobalProperty(Properties.PROP_LOG_LEVEL, Log.DEBUG_LEVEL_INFO);
            // set device ip address explicitly on server spot if basestation is involved
	    
	 //address altered from 127.0.0.1 - johannes
	    
            Properties.getInstance().setGlobalProperty(Properties.PROP_DEVICE_IP_ADDRESS,"192.168.1.126");
            // use 254 as http port because both spot and basestation can open a server connection on it
	    Properties.getInstance().setGlobalProperty(Properties.PROP_HTTP_SERVER_PORT, "255");
	    // set broadcast address - this indicates that multicast has to be done via broadcast (CLDC does not support multicast)
	    Properties.getInstance().setGlobalProperty(Properties.PROP_BROADCAST_ADDRESS, "broadcast:82");	   
	    // set SunSPOTLocalToolkit
	    PlatformToolkit.setToolkit(new SunSPOTLocalToolkit());
	    
	    // create DPWS device
            HelloWorldDevice device =     new HelloWorldDevice();
            // create service
	    HelloWorldService service =   new HelloWorldService();
	    // set device properties
	    Properties.getInstance().setDeviceProperty(device, Properties.PROP_DEVICE_UUID, "urn:uuid:df0794e0-40d5-11dc-9999-40f84sunspot" );
	    // add service to device
	    device.addHostedService(service);
	    // start device
	    device.start();
	    
      
	    ISwitch sw1 = EDemoBoard.getInstance().getSwitches()[EDemoBoard.SW1];
	    ISwitch sw2 = EDemoBoard.getInstance().getSwitches()[EDemoBoard.SW2];
	    
	    leds[6].setRGB(0, 0, 100);  //blau
	    leds[7].setRGB(100, 0, 0);  //rot
	    leds[6].setOff();
	    leds[7].setOff();
	    
	    // fake schleifen z�hler
	    int i = 0;
	    while (i<1000) {    // sw1.isOpen()      // leave when switch is pressed
	    	leds[7].setOn();                    // Blink LED
	    	Utils.sleep(250);                   // wait 1/4 seconds
	    	leds[7].setOff();
	    	Utils.sleep(1000);                  // wait 1 second	
		if (sw1.isClosed())
		{
		    leds[6].setOn();
		    device.sendHello();
		    Utils.sleep(2000);		    // wait 3 seconds
		    leds[6].setOff();
		    i++;
		}
	    }
	    
	    device.stop();
	    notifyDestroyed();                      // cause the MIDlet to exit
    }
    
    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }
    
    /**
     * Called if the MIDlet is terminated by the system.
     * I.e. if startApp throws any exception other than MIDletStateChangeException,
     * if the isolate running the MIDlet is killed with Isolate.exit(), or
     * if VM.stopVM() is called.
     * 
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     *    cleanup and release all resources. If false the MIDlet may throw
     *    MIDletStateChangeException  to indicate it does not want to be destroyed
     *    at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < 8; i++) {
            leds[i].setOff();
        }
    }
}
