/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package ws4dtest;

import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ISwitch;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import com.sun.spot.util.Utils;
import org.ws4d.java.discovery.ISearchResult;
import org.ws4d.java.service.HostedService;
import org.ws4d.java.util.Properties;
import org.ws4d.java.xml.QualifiedName;

/**
 * HelloWorldService describes the service that is hosted by the SPOT. 
 * In particular the service has one method HelloWorldAction.
 *
 */
public class HelloWorldService extends HostedService {
	// global settings
    
	public static final String    NAMESPACE     = HelloWorldDevice.NAMESPACE;
	public static final String    PORTTYPE      = "SensorInterface";
	public static final String    ENDPOINT_PATH = "/SunSPOTSensor";
	public static final QualifiedName QN_PORTTYPE = new QualifiedName(PORTTYPE, NAMESPACE);
	
	public HelloWorldService() {
	       super();
	       // Set the property
	       Properties.getInstance().setServiceProperty(this, Properties.PROP_SERVICE_SECURED, false);
	       // if endpoint not set, port type is use
	       setEndpointPath(ENDPOINT_PATH);
	       // -- hello world action --
	       //HelloWorldAction helloWorldAct = new HelloWorldAction ();
	       //addAction(helloWorldAct);
	       
	       // -- getTemperatureAction --
	       GetTemperatureAction getTemperatureAct = new GetTemperatureAction();
	       addAction(getTemperatureAct);
	       
	       // -- getValuesAction --
	       GetLightAction getLightAct = new GetLightAction();
	       addAction(getLightAct);
	       
	       // -- getValuesAction --
	       //GetValuesAction getValuesAct = new GetValuesAction();
	       //addAction(getValuesAct);
	       
	       
	       
	}
}
