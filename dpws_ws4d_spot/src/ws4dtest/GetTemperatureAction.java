/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package ws4dtest;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.resources.transducers.ITemperatureInput;
import java.io.IOException;
import org.ws4d.java.communication.DPWSException;
import org.ws4d.java.service.Action;
import org.ws4d.java.service.Parameter;
import org.ws4d.java.service.ParameterType;

/**
 * The Temperature Action returns the current Temperature.
 * 
 * 
 */
public class GetTemperatureAction extends Action
{
        private ITemperatureInput tempSensor = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
	//private ILightSensor lightSensor = (ILightSensor) Resources.lookup(ILightSensor.class);
		
	public static final String ACT_HW_NAME     = "TemperatureAction";
	public static final String PARAM_HW_OUTPUT = "temperature";
	//public static final String PARAM_HW_OUTPUT2 = "light";
	
	public GetTemperatureAction () {
		super(ACT_HW_NAME, HelloWorldService.QN_PORTTYPE, false);
		Parameter temperatureOutput = new Parameter(PARAM_HW_OUTPUT, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		//Parameter lightOutput = new Parameter(PARAM_HW_OUTPUT2, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		
		addOutputParameterDefinition(temperatureOutput);
		//addOutputParameterDefinition(lightOutput);
       
	}
	
	public void invoke() throws DPWSException {
        try {
            Parameter temperatureOutput = getOutputParameter(PARAM_HW_OUTPUT);
            //Parameter lightOutput = getOutputParameter(PARAM_HW_OUTPUT2);
	    
            // The DPWS stack handles all simple type parameters as strings. The developer is
            // responsible to check any constraints and to perform any necessary type conversions.
            
            // business logic
            double temperature = tempSensor.getCelsius();
            temperatureOutput.setValue(""+temperature);
	    
	    //double light = lightSensor.getAverageValue();
	    //lightOutput.setValue(""+light);
            }
        catch (IOException ex) {}
	}
}
