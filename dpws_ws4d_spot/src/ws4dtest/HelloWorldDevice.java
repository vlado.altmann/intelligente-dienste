/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package ws4dtest;


import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import java.io.OutputStream;
import java.util.Hashtable;
import org.ws4d.java.communication.DPWSException;
import org.ws4d.java.communication.UniformResourceIdentifier;
import org.ws4d.java.communication.http.HTTPHeader;
import org.ws4d.java.communication.http.HTTPParameter;
import org.ws4d.java.discovery.ISearchResult;
import org.ws4d.java.service.AbstractAction;
import org.ws4d.java.service.HostingService;
import org.ws4d.java.xml.QualifiedName;
import org.ws4d.java.xml.XMLElement;

/**
 * HelloWorldDevice describes the SPOT as a DPWS hosting device.
 *
 * modified by Christin Groba
 * 
 */
public class HelloWorldDevice extends HostingService{
	public static final String   NAMESPACE      = "http://www.demo.com/bbsr";
	public static final String   PORTTYPE       = "Sensor";
	public static final String   LOCALE_EN      = "en_GB";
	
	public HelloWorldDevice() {
		super( new QualifiedName(PORTTYPE, NAMESPACE ) );		
		setManufacturerName( LOCALE_EN, "Sun microsystems" );
		setFriendlyName( LOCALE_EN, "Sun SPOT - Sensor Device" );
		setModelName( LOCALE_EN, "Sun SPOT" );
	}	
}
