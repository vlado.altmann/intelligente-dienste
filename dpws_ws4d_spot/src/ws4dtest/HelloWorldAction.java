/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package ws4dtest;

import org.ws4d.java.communication.DPWSException;
import org.ws4d.java.service.Action;
import org.ws4d.java.service.Parameter;
import org.ws4d.java.service.ParameterType;

/**
 * HelloWorldAction adds two integer a and b and returns the sum.
 * 
 * modified by Christin Groba
 *
 */
public class HelloWorldAction extends Action{
	public static final String ACT_HW_NAME     = "HelloWorldAddAction";
	public static final String PARAM_HW_INPUTA  = "a";
	public static final String PARAM_HW_INPUTB  = "b";
	public static final String PARAM_HW_OUTPUT = "result";
	
	public HelloWorldAction () {
		super(ACT_HW_NAME, HelloWorldService.QN_PORTTYPE, false);
		Parameter helloWorldInputA = new Parameter(PARAM_HW_INPUTA, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		Parameter helloWorldInputB = new Parameter(PARAM_HW_INPUTB, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		Parameter helloWorldOutput = new Parameter(PARAM_HW_OUTPUT, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		
		addInputParameterDefinition(helloWorldInputA);
		addInputParameterDefinition(helloWorldInputB);
		addOutputParameterDefinition(helloWorldOutput);
       
	}
	
	public void invoke() throws DPWSException {
		Parameter helloWorldInputA = getInputParameter(PARAM_HW_INPUTA);
		Parameter helloWorldInputB = getInputParameter(PARAM_HW_INPUTB);
		Parameter helloWorldOutput = getOutputParameter(PARAM_HW_OUTPUT);
		
		// The DPWS stack handles all simple type parameters as strings. The developer is
		// responsible to check any constraints and to perform any necessary type conversions.
		int a = Integer.valueOf(helloWorldInputA.getValue()).intValue() ;
		int b = Integer.valueOf(helloWorldInputB.getValue()).intValue() ;
		
		// business logic
		int result = a+b;
		helloWorldOutput.setValue(""+result);
	}
}
