/**********************************************************************************
 * Copyright (c) 2007 MATERNA Information & Communications and
 *   TU Dortmund, Dpt. of Computer Science, Chair 4, Distributed Systems
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 **********************************************************************************/

package ws4dtest;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.sensorboard.peripheral.ITriColorLED;
import java.io.IOException;
import org.ws4d.java.communication.DPWSException;
import org.ws4d.java.service.Action;
import org.ws4d.java.service.Parameter;
import org.ws4d.java.service.ParameterType;

/**
 * The Temperature Action returns the current Temperature.
 * 
 * 
 */
public class GetLightAction extends Action
{
        //private ITemperatureInput tempSensor = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
	private ILightSensor lightSensor = (ILightSensor) Resources.lookup(ILightSensor.class);
	private ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();
	
	
	public static final String ACT_HW_NAME     = "LightAction";
	//public static final String PARAM_HW_OUTPUT = "temperature";
	public static final String PARAM_HW_OUTPUT = "light";
	
	public GetLightAction () {
		super(ACT_HW_NAME, HelloWorldService.QN_PORTTYPE, false);
		//Parameter temperatureOutput = new Parameter(PARAM_HW_OUTPUT, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		Parameter lightOutput = new Parameter(PARAM_HW_OUTPUT, HelloWorldDevice.NAMESPACE, ParameterType.PARAMETER_TYPE_INT);
		
		//addOutputParameterDefinition(temperatureOutput);
		addOutputParameterDefinition(lightOutput);
       
	}
	
	public void invoke() throws DPWSException
	{
	    
            //Parameter temperatureOutput = getOutputParameter(PARAM_HW_OUTPUT);
            Parameter lightOutput = getOutputParameter(PARAM_HW_OUTPUT);
	    
            // The DPWS stack handles all simple type parameters as strings. The developer is
            // responsible to check any constraints and to perform any necessary type conversions.
            
            // business logic
            //double temperature = tempSensor.getCelsius();
            //temperatureOutput.setValue(""+temperature);
	    
	    double light = 0.0;
	try{light = lightSensor.getAverageValue();} catch (IOException ex) {}
	    lightOutput.setValue(""+light);
	    
	    
        }
}
