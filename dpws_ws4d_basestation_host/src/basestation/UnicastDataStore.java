package basestation;

import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * UnicastDataStore is a data store that manages the unicast addresses and ports.
 * 
 * @author Christin Groba
 * 
 */
public class UnicastDataStore {
	private InetAddress unicastAddr;
	private int unicastPort;
        private MulticastSocket socket;
	private static UnicastDataStore instance = new UnicastDataStore();
	
	private UnicastDataStore(){
		
	}
	
	public static UnicastDataStore getInstance() {
		return instance;
	}

	public synchronized void setUnicastAddr(InetAddress address) {
		this.unicastAddr = address;
	}

	public synchronized InetAddress getUnicastAddr() {
		return unicastAddr;
	}

	public synchronized void setUnicastPort(int port) {
		this.unicastPort = port;
	}

	public synchronized int getUnicastPort() {
		return unicastPort;
	}
        
        public synchronized void setMulticastSocket(MulticastSocket newSocket)
        {
            socket = newSocket;
        }

        public synchronized MulticastSocket getMulticastSocket()
        {
            return socket;
        }
}
