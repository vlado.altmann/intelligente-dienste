package basestation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Vector;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import java.net.MulticastSocket;


/**
 * RadioUnicastHandler receives radio unicast messages over UDP (i.e., as radiograms)
 * and forwards them to the IP node as unicast messages over UDP (i.e., as datagrams). 
 * 
 * The forwarded message are ResolveMatch and ProbeMatch.
 * 
 * @author Christin Groba
 *
 */
public class RadioUnicastHandler implements Runnable {
	private static final String 	UNICASTADDRESS 	= 	"radiogram://:32";
	private static final int	MULTICASTPORT	=	3702;	//edit by Vlado
	private RadiogramConnection conn = null;
	private Datagram dg = null;

	public void run() {
		Vector<byte[]> msgVector = new Vector<byte[]>();
		int msgSize = 0;
		byte[] message = null;
		
		// open radio unicast receiver (server)
		while(conn == null){
			try {
				conn = (RadiogramConnection) Connector.open(UNICASTADDRESS);
				System.out.println("RadioUnicastHandler listens on: "+UNICASTADDRESS);
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		
		// create datagram
		while(dg == null){
			try {
				dg = conn.newDatagram(conn.getMaximumLength());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// listen for messages and forward them	
		while (true) {
			// initialize
			msgVector.clear();
			msgSize = 0;
			
			// receive maybe fragmented packets
			while(true){
				dg.reset();
				try {
					conn.receive(dg);
					byte[] b = new byte[dg.getLength()];
					dg.readFully(b);
					// add to msgVector - continuation bit is still included
					msgVector.add(b);
					// System.out.println("dg received: "+new String(b));
					// msgSize is without continuation bit
					msgSize = msgSize + (b.length-1);
					// check continuation bit
					int cb = b[(b.length-1)];
					if(cb==0){
						// create complete array
						message = joinMessageFragments(msgVector, msgSize);
						break;
					}
					// System.out.println("RadioUnicastHandler waits for next part of the message");
				} catch (IOException e) {
					e.printStackTrace();
					break;
				}
				
			}			
			
			// uncommented following 2 lines
			System.out.println("Received unicast data from "+dg.getAddress()+ " with length: " + message.length);
			System.out.println(new String(message));
			
			/**if(new String(message).contains("ProbeMatches"))
			{
			    System.out.println("--> --> --> ProbeMatch detected");
			}
			**/
			
			// forward unicast meassage
			forward(message);
		}
	}
	
	private void forward(byte[] message){
		int port = UnicastDataStore.getInstance().getUnicastPort();
		InetAddress address = UnicastDataStore.getInstance().getUnicastAddr();
		
		// open UDP client socket
		try {			
			//DatagramSocket socket = new DatagramSocket();
                        MulticastSocket socket = UnicastDataStore.getInstance().getMulticastSocket();
                        System.out.println("Forward unicast message over udp to "+address+":"+port + " from IP: " + 
                                socket.getLocalAddress().getHostAddress() + ":" + socket.getLocalPort());
			DatagramPacket out = new DatagramPacket(message, message.length, address, port);
			socket.send(out);
			//socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private byte[] joinMessageFragments(Vector<byte[]> parts, int size){
		byte[] message = new byte[size];
		int destPos = 0;
		
		for (Enumeration<byte[]> el = parts.elements(); el.hasMoreElements();) {
			byte[] part = (byte[]) el.nextElement();
			// copy without continuation bit
			System.arraycopy(part, 0, message, destPos, (part.length-1));
			destPos = destPos + (part.length -1);
		}
		return message;
	}

}
