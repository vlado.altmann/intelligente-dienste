package basestation;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.io.j2me.radiostream.RadiostreamConnection;

/**
 * HttpHandler receives unicast messages over TCP from a IP node (i.e., datastream) and
 * forwards them as radio unicast messages to a Sun SPOT (i.e., radiostream). Then 
 * the handler waits for a response from the SPOT and forwards it to the IP node.
 * 
 * The forwarded message are Get (send from IP node) and GetResponse (send from SPOT).
 * 
 * Currently, for sake of an this example, the SPOT address is hard coded.
 * 
 * @author Christin Groba
 *
 */
public class HttpHandler implements Runnable {
	private final int SOCKET_PORT = 255;	
	private final String PEERCONNECTIONREQUEST = "ConnectMe";
	private final int SPOT_HTTP_PORT = 255;
	private final String spotaddress;
	
	public HttpHandler(String spotaddress){
		this.spotaddress = spotaddress;
	}
	
	public void run() {
		String request;
		String response;
		ServerSocket serverSocket = null;
		Socket socket = null;
		InputStream in  = null;
		OutputStream out = null;
		
		try {
			// open tcp server socket
			serverSocket = new ServerSocket(SOCKET_PORT);
			while (true) {
				System.out.println("HttpHandler will talk to SPOT "+spotaddress);
				System.out.println("HttpHandler waits for connect on: "	+ serverSocket.getLocalPort());
				socket = serverSocket.accept();
				System.out.println("HttpHandler received connect on: " 	+ serverSocket.getLocalPort());
				
//				// read request
//				in = socket.getInputStream();
//				ByteArrayOutputStream buf = new ByteArrayOutputStream();
//				int result;
//				while(true){
//					result = in.read(); 	// this blocks until there is something to read
//					byte b = (byte) result;
//					buf.write(b);
//					if(in.available()==0){
//						break;
//					}	
//				}
				
				in = socket.getInputStream();
				ByteArrayOutputStream buf = new ByteArrayOutputStream();
                                //while (in.available() < 300) continue;
                                while (buf.toString().contains("<?xml version=\"1.0\"") == false) {
                                    while (true) {
                                        byte b = (byte) in.read(); 	// this blocks until there is something to read
                                        buf.write(b);
                                        if (in.available() == 0) {
                                            break;
                                        }
                                    }
                                }
				request = buf.toString();				
				// System.out.println("Basestation received request from client: " + request);
				
				// forward request
				response = forward(request);
				// System.out.println("Basestation received response from spot: " + response);
				
				// read response
				out =  socket.getOutputStream();
				out.write(response.getBytes());
				out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private synchronized RadiostreamConnection openRadioStreamConnection(String address, int port) throws IOException {
		int peerport = -1;
		RadiogramConnection requestconn, replyconn;
		
		// open a new datagram connection where the peerPort will be received
		// it has to be a new connection and not the send connection because otherwise this method would not receive the reply but the HTTPSever
		replyconn = (RadiogramConnection) Connector.open("radiogram://");
		int replyport = replyconn.getLocalPort();
		//System.out.println("Open radiogram listener for stream port on port: "+replyport);
		
		// open a radiogram connection to request the peer connection request
		requestconn = (RadiogramConnection) Connector.open("radiogram://" + address + ":" + port);
		Datagram dg = requestconn.newDatagram(requestconn.getMaximumLength());
		dg.reset();
		dg.writeUTF(PEERCONNECTIONREQUEST+":"+replyport);
		requestconn.send(dg);
		requestconn.close();
		
		// listen on the replyconn for the peerport on which to create the stream connection
		Datagram dgreply = replyconn.newDatagram(replyconn.getMaximumLength());
		dgreply.reset();
		replyconn.receive(dgreply);
		peerport = dgreply.readInt();
		// System.out.println("Stream port received: "+peerport);			
		replyconn.close();

		// create stream connection
		RadiostreamConnection radioStreamConnection = (RadiostreamConnection) Connector.open("radiostream://" + address + ":" + peerport);

		return radioStreamConnection;
	}

	private String forward(String message) throws IOException {
		RadiostreamConnection radioStreamConnection = null;
		DataOutputStream radioStreamDos = null;
		DataInputStream radioStreamDis = null;
		
		// each time open radiostream connection new
		radioStreamConnection = openRadioStreamConnection(spotaddress, SPOT_HTTP_PORT);
		radioStreamDos = radioStreamConnection.openDataOutputStream();
		radioStreamDis = radioStreamConnection.openDataInputStream();
		
		// send message
		radioStreamDos.write(message.getBytes());
		radioStreamDos.flush();

		// receive response
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		int result = 0;
		while((result=radioStreamDis.read())> -1){
			byte b = (byte) result;
			buf.write(b);
			// System.out.println((char)result);
			if(radioStreamDis.available()==0){
				//wait a bit before deciding if we really want to break
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(radioStreamDis.available()==0){
					break;
				}
			}
		}
		
		// close stream connection		
		radioStreamDos.close();
		radioStreamDis.close();
		radioStreamConnection.close();
		
		return buf.toString();
	}
}
