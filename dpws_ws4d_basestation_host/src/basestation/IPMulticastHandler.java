package basestation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;

/**
 * IPMulticastHandler receives multicast messages from IP client and
 * forwards them as radio broadcast messages to the Sun SPOTs.
 * 
 * The forwarded message are Hello, Bye, Resolve, and Probe (send from IP node).
 * 
 * @author Christin Groba
 *
 */
public class IPMulticastHandler implements Runnable {
	private static final String 	BROADCASTADDRESS 	= 	"radiogram://broadcast:82";
	private static final String 	MULTICASTGROUP 		=	"239.255.255.250";
	private static final int 		MULTICASTPORT 		= 	3702;
	private RadiogramConnection conn = null;
	private Datagram dg = null;
	
	private void forward(byte[] message){
		try {
			// open radio broadcast sender
			conn = (RadiogramConnection) Connector.open(BROADCASTADDRESS);
			dg = conn.newDatagram(conn.getMaximumLength());		
			
			// keep within radiogram size
			int max = conn.getMaximumLength();
			int off = 0;
			int len = max-1;
			int fragments;
			int counter = 0;
			
			// calculate number of fragments;
			fragments = message.length/(max-1);
			if((message.length % (max-1))!=0) fragments++; //round up
			
			// System.out.println("Forward multicast data in fragments: "+fragments);
			
			// send message in several fragments if necessary
			for(counter=0; counter < fragments; counter++){
				dg.reset();
				if(counter==fragments-1){
					//last slice
					dg.write(message, off, message.length-off);
					dg.write(0);
				}else{
					dg.write(message, off, len);
					dg.write(1);
				}
				conn.send(dg);
				off = off +len;
			}
			conn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		try {
			// open multicast server connection
			MulticastSocket serversocket = new MulticastSocket(MULTICASTPORT);
			serversocket.joinGroup(InetAddress.getByName(MULTICASTGROUP));		
			
			// set buffer to max length a multicast datagram can have on this platform (but this is only a placeholder)
			byte buffer[] = new byte[serversocket.getReceiveBufferSize()];
			DatagramPacket pack = new DatagramPacket(buffer, buffer.length);
			
			System.out.println("IPMulticastHandler listens on: " + MULTICASTGROUP + ":" + MULTICASTPORT);
			UnicastDataStore.getInstance().setMulticastSocket(serversocket);

			while(true){
				serversocket.receive(pack);
				UnicastDataStore.getInstance().setUnicastAddr(pack.getAddress());
				UnicastDataStore.getInstance().setUnicastPort(pack.getPort());
				
				// only some printouts for debugging
				// System.out.println("Received multicast data from: " + pack.getAddress().toString() + ":" + pack.getPort() + " with length: " + pack.getLength());
				// System.out.write(pack.getData(),0,pack.getLength());
				// System.out.println();
				
				// forward only real content not the supersized buffer
				byte[] content = new byte[pack.getLength()];
				System.arraycopy(pack.getData(), 0, content, 0, pack.getLength());
				forward(content);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
