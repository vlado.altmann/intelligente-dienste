package basestation;


/**
 * GatewayApplication bridges the communication between an IP network and a IEEE.802.15.4 based 
 * network. It creates handler for http, ip multicast, radio broadcast, and radio unicast 
 * messages.
 * 
 * @author Christin Groba
 *
 */
public class GatewayApplication {
    //leere zeile
	
	public void startGateway(String spotaddress){
		Thread radiobroadcasthandler = new Thread(new RadioBroadcastHandler());
		radiobroadcasthandler.start();
		
		Thread ipmulticasthandler = new Thread(new IPMulticastHandler());
		ipmulticasthandler.start();
		
		Thread httpHandler = new Thread(new HttpHandler(spotaddress));
		httpHandler.start();
		
		Thread radiounicasthandler = new Thread(new RadioUnicastHandler());
		radiounicasthandler.start();
	}

	public static void main(String[] args) throws Exception {
		String spotaddress = args[0];
		GatewayApplication app = new GatewayApplication();
		app.startGateway(spotaddress);
	}

}
