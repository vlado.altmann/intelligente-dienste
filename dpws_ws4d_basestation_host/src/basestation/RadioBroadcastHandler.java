package basestation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;

/**
 * RadioBroadcastHandler receives radio broadcast messages from a Sun SPOT
 * and forwards them to a IP node as multicast messages.
 * 
 * The forwarded message are Hello and Bye (send from the SPOT).
 * 
 * @author Christin Groba
 *
 */
public class RadioBroadcastHandler implements Runnable{
	private static final String 	BROADCASTADDRESS 	= 	"radiogram://:82";
	private static final String 	MULTICASTGROUP 		=	"239.255.255.250";
	private static final int 		MULTICASTPORT 		= 	3702;	
	private RadiogramConnection conn = null;
	private Datagram dg = null;
	
	private void forward(byte[] message) {	
		try {
			// create a mulitcast client socket
			MulticastSocket s = new MulticastSocket();
			DatagramPacket pack = new DatagramPacket(message, message.length, InetAddress.getByName(MULTICASTGROUP), MULTICASTPORT);
			s.send(pack);
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		// open radio broadcast receiver
		while(conn == null){
			try {
				conn = (RadiogramConnection) Connector.open("radiogram://:82");
				System.out.println("RadioBroadcastHandler listens on: "+BROADCASTADDRESS);
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
		
		// create datagram
		while(dg == null){
			try {
				dg = conn.newDatagram(conn.getMaximumLength());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// listen for messages from SPOT and forward them to IP-Client	
		while (true) {
			dg.reset();
			try {
				conn.receive(dg);
				byte[] b = new byte[dg.getLength()];
				dg.readFully(b);
				// check continuation bit
				int c = b[b.length-1];
				if(c==1){
					//TODO: wait for next part
				}else{
					//TODO: last part received
				}
				// remove continuation bit
				byte[] message = new byte[b.length-1];
				for(int i=0; i<b.length-1; i++){
					message[i]= b[i];
				}
				
				//uncommented following 2 lines
				 System.out.println("Received broadcast data from "+dg.getAddress()+ " with length: " + dg.getLength());
				 System.out.println(new String(message));
				
				// forward broadcast meassage to IP-Client
				forward(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
