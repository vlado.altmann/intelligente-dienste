package com.pi.smartwindow.device;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.Enumeration;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

public class SmartWindowServiceProvider {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("Working Path: " + System.getProperty("user.dir"));

		try {			
			KeyStore ks = KeyStore.getInstance("JKS");

			// get user password and file input stream
			char[] password = "111111".toCharArray();

			java.io.FileInputStream fis = null;

			fis = new java.io.FileInputStream("client.jks");
			ks.load(fis, password);
			fis.close();


			System.out.println("Keystore aliases:");
			Enumeration<String> aliases = ks.aliases();
			for (String alias : Collections.list(aliases)) {
				System.out.println(alias);
			}

			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			System.setProperty("javax.net.ssl.trustStore", "client.jks");
			System.setProperty("javax.net.ssl.trustStorePassword", "111111");
			
			Authenticator.setDefault (new Authenticator() {
			    protected PasswordAuthentication getPasswordAuthentication() {
			        return new PasswordAuthentication ("dssadmin", "dssadmin".toCharArray());
			    }
			});

		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
//		properties.setDiscoveryButton0(1);
//		properties.setDiscoveryButton1(3);
		properties.addSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION2006));
//		properties.removeSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2009.DPWS_VERSION2009));
		DispatchingProperties.getInstance().setResponseWaitTime(10000);
		
		// mandatory: Starting the DPWS Framework.
		JMEDSFramework.start(null);
		
		// First we need a device.
		SmartWindowDevice device = new SmartWindowDevice();

		// Then we create a service.
		final SmartWindowService service = new SmartWindowService();
		
		if (args.length > 0)
		{
			service.setWindowAddress(args[0]);
		}

		// In the end we add our service to the device.
		device.addService(service);

		// Do not forget to start the device!
		try {
			device.start();
//			Thread.sleep(5000);
//			client.resetPairing();
//			client.searchKnownDevices();
			GpioListener.getInstance().registerDevice(device);
			GpioListener.getInstance().activateHello(true);
			
			service.closeWindow();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
