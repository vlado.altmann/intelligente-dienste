package com.pi.smartwindow.device;

import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;

/**
 * Implementation of the service described in "An Introduction to WS4D and to
 * JMEDS, a framework for distributed communication between devices in a domotic
 * environment" by Pierre-Alexandre Gagn�
 * 
 * @author ajordan
 */
public class SmartWindowService extends DefaultService {

	public final static URI	SERVICE_ID	= new URI("SmartWindowService");
	public final static String SERVICE_TYPE = "SmartWindowInterface";
	private SwitchOperation switchOp;

	/**
	 * Standard Constructor
	 */
	public SmartWindowService() {
		super();

		this.setServiceId(SERVICE_ID);

		// add Operations to the service		
		GetStateOperation getState = new GetStateOperation();
		addOperation(getState);

		switchOp = new SwitchOperation();
		addOperation(switchOp);
		
		switchOp.setGetStateOperation(getState);

	}
	
	public void closeWindow()
	{
		SwitchOperation op = (SwitchOperation) getOperation(null, "Switch", null, null);
		op.closeWindow();
	}
	
	public void setWindowAddress(String address)
	{
		switchOp.setWindowAddress(address);
	}

}
