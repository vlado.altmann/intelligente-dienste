package com.pi.smartwindow.device;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

public class SwitchOperation extends Operation {

	private GetStateOperation getOperation;
	private String windowAddress = "139.30.202.50:8080";

	public SwitchOperation() {
		super("Switch", new QName(SmartWindowService.SERVICE_TYPE, SmartWindowDevice.NAMESPACE));

		// create element of type TemperatureType
		Element switchElement = new Element(new QName("switch", SmartWindowDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set the input of the operation
		setInput(switchElement);

		// create reply element
		Element reply = new Element(new QName("status", SmartWindowDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set this element as output
		setOutput(reply);
		
	}

	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		// get string value from input
		String value = ParameterValueManagement.getString(parameterValue, "switch");
		
		// create output and set value
		ParameterValue result = createOutputValue();
		
		if (value.equals("open"))
		{			
			Log.info("Opening window");
			if (openWindow())
			{
				getOperation.setState(value);
				ParameterValueManagement.setString(result, "status", value);
			}
			else
			{
				ParameterValueManagement.setString(result, "status", "error");
			}
		}
		else if (value.equals("close"))
		{
			Log.info("Closing window");
			if (closeWindow())
			{
				getOperation.setState(value);
				ParameterValueManagement.setString(result, "status", value);
			}
			else
			{
				ParameterValueManagement.setString(result, "status", "error");
			}
		}
		else
		{
			ParameterValueManagement.setString(result, "status", "error");
		}

		return result;
	}

	public boolean openWindow()
	{
		try {						
			URL url = new URL("https://" + windowAddress + "/json/zone/callScene?id=65535&groupID=1&sceneNumber=0");
			URLConnection yc = url.openConnection(); 

			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) 
				System.out.println(inputLine);
			in.close();
			return true;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        

		return false;
	}

	public boolean closeWindow()
	{
		try {
			URL url = new URL("https://" + windowAddress + "/json/zone/callScene?id=65535&groupID=1&sceneNumber=5");
			URLConnection yc = url.openConnection();

			BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) 
				System.out.println(inputLine);
			in.close();
			return true;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        

		return false;
	}
	
	public void setGetStateOperation(GetStateOperation op)
	{
		getOperation = op;
	}
	
	public void setWindowAddress(String address)
	{
		windowAddress = address;
	}
}
