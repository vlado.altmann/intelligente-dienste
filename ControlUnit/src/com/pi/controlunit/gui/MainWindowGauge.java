/**
 * 
 */
package com.pi.controlunit.gui;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;

import eu.hansolo.steelseries.gauges.Linear;
import eu.hansolo.steelseries.extras.Battery;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import eu.hansolo.steelseries.gauges.RadialBargraph;
import eu.hansolo.steelseries.tools.GaugeType;
import javax.swing.JButton;

import org.ws4d.java.pi.gpio.ButtonListener;
import org.ws4d.java.pi.gpio.GpioListener;

import java.awt.Color;

/**
 * @author va015
 *
 */
public class MainWindowGauge extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private RadialBargraph gauge;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		        
		// GUI Start
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainWindowGauge thisClass = new MainWindowGauge();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
		
		
	}

	/**
	 * This is the default constructor
	 */
	public MainWindowGauge() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(814, 674);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
//		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		GraphicsDevice device = GraphicsEnvironment
			.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.isFullScreenSupported())
		{				
			this.dispose();			
			this.setUndecorated(true);
			this.setVisible(true);
			device.setFullScreenWindow(this);
		}
	}
	
	private void hideCursor()
	{
		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = java.awt.Toolkit.getDefaultToolkit().createCustomCursor(
		    cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		this.getContentPane().setCursor(blankCursor);
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setBackground(Color.BLACK);
			
			gauge = new RadialBargraph();
			gauge.setGaugeType(GaugeType.TYPE2);
			gauge.setValueAnimated(24);
//			gauge.setLedBlinking(true);
			gauge.setDigitalFont(true);
//			gauge.setUserLedOn(true);
			gauge.setTitle("Temperature");
			gauge.setUnitString("�C");
			GroupLayout gl_jContentPane = new GroupLayout(jContentPane);
			gl_jContentPane.setHorizontalGroup(
				gl_jContentPane.createParallelGroup(Alignment.LEADING)
					.addComponent(gauge, GroupLayout.PREFERRED_SIZE, 1280, GroupLayout.PREFERRED_SIZE)
			);
			gl_jContentPane.setVerticalGroup(
				gl_jContentPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_jContentPane.createSequentialGroup()
						.addComponent(gauge, GroupLayout.PREFERRED_SIZE, 800, GroupLayout.PREFERRED_SIZE)
						.addGap(101))
			);
			jContentPane.setLayout(gl_jContentPane);
		}
		hideCursor();
		return jContentPane;
	}
	
	public void incValue()
	{
		double value = gauge.getValue();
		gauge.setValueAnimated(value + 1);
	}
	
	public void decValue()
	{
		double value = gauge.getValue();
		gauge.setValueAnimated(value - 1);
	}
	
	public void setStatus(String status)
	{
		
	}

	public void setIndoorTemp(int value, String unit) {
		gauge.setValueAnimated(value);
		gauge.setUnitString("�" + unit);
	}
}
