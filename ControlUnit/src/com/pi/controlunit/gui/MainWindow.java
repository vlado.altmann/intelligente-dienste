package com.pi.controlunit.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.swing.JSeparator;

import org.ws4d.java.pi.gpio.ButtonListener;
import org.ws4d.java.pi.gpio.GpioListener;
import javax.swing.SwingConstants;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JLabel indoorLabel;
	private JLabel statusLabel;
	private int indoorTemp = 24;
	private String indoorUnit = "C";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
//		this.setSize(814, 674);
//		this.setContentPane(getJContentPane());
		this.setTitle("Monitor");
//		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		GraphicsDevice device = GraphicsEnvironment
			.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (device.isFullScreenSupported())
		{				
			this.dispose();			
//			this.setUndecorated(true);
			this.setVisible(true);
			device.setFullScreenWindow(this);
		}
	}
	
	private void hideCursor()
	{
		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = java.awt.Toolkit.getDefaultToolkit().createCustomCursor(
		    cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		this.getContentPane().setCursor(blankCursor);
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		initialize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(-10, -30, 422, 307);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		hideCursor();
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel label_3 = new JLabel("Indoor Temperature");
		label_3.setFont(new Font("OCR A Extended", Font.PLAIN, 26));
		label_3.setForeground(new Color(34, 139, 34));
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridwidth = 8;
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 0;
		gbc_label_3.gridy = 1;
		contentPane.add(label_3, gbc_label_3);
		
		indoorLabel = new JLabel("" + indoorTemp + "\u00B0" + indoorUnit);
		indoorLabel.setHorizontalAlignment(SwingConstants.CENTER);
		indoorLabel.setFont(new Font("OCR A Extended", Font.PLAIN, 99));
		indoorLabel.setForeground(new Color(34, 139, 34));
		GridBagConstraints gbc_indoorLabel = new GridBagConstraints();
		gbc_indoorLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_indoorLabel.insets = new Insets(0, 0, 5, 5);
		gbc_indoorLabel.gridheight = 3;
		gbc_indoorLabel.gridwidth = 8;
		gbc_indoorLabel.gridx = 0;
		gbc_indoorLabel.gridy = 2;
		contentPane.add(indoorLabel, gbc_indoorLabel);
		
		statusLabel = new JLabel("not connected");
		statusLabel.setForeground(new Color(30, 144, 255));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.NORTH;
		gbc_label.gridwidth = 8;
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 0;
		gbc_label.gridy = 5;
		contentPane.add(statusLabel, gbc_label);
		
	}
	
	public void setIndoorTemp(int temp, String unit)
	{
		if (unit == null)
		{
			indoorLabel.setText("" + temp + "\u00B0C");	
		}
		else
		{
			indoorLabel.setText("" + temp + "\u00B0" + unit.toUpperCase());
		}
		
	}
	
	public void setStatus(String status)
	{
		statusLabel.setText(status);
	}

}
