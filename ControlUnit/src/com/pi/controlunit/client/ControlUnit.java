package com.pi.controlunit.client;

import java.awt.EventQueue;
import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.util.Log;

import com.pi.controlunit.gui.MainWindow;

public class ControlUnit {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
		properties.setDiscoveryButton0(0);
		properties.setDiscoveryButton1(3);
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
//		properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);
		DispatchingProperties.getInstance().setResponseWaitTime(10000);
		
		// mandatory: Starting the DPWS Framework.
		JMEDSFramework.start(args);
		
		final ControlUnitClient client = new ControlUnitClient();

		client.registerKnownHello();
		GpioListener.getInstance().registerClient(client);
		GpioListener.getInstance().activateProbe(true);
		
		//GUI start
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					client.setGui(frame);
//					client.searchKnownDevices();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}
