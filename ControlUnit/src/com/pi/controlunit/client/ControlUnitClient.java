package com.pi.controlunit.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.authorization.AuthorizationException;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.dispatch.DefaultServiceReference;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.pi.gpio.ButtonListener;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.types.HelloData;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.communication.connection.ip.IPAddress;

import com.pi.controlunit.gui.MainWindow;
import com.pi.controlunit.gui.MainWindowGauge;

public class ControlUnitClient extends DefaultClient implements ButtonListener {
	public final static String NAMESPACE	= "http://www.demo.com/bbsr";

	public final static String DEVICE		= "AirConditioner";
	
	public final static String SERVICE 		= "AirCondInterface";
	
	boolean bound							= false;
	
	private Service service;
	
	private MainWindow gui					= null;
	
	private boolean requested				= false;
	
	private ClientSubscription	notificationSub	= null;
	
	private int temperature = 24;
	
	private String unit = "C";
	
	public ControlUnitClient()
	{
		GpioListener.getInstance().addListener(this);
	}
	
	@Override
	public synchronized void deviceFound(DeviceReference devRef, SearchParameter search) {
		QNameSet types = devRef.getDiscoveryData().getTypes();
		org.ws4d.java.structures.Iterator i = types.iterator();
//		System.out.println("Found Device: " + devRef.toString() + ": bound = " + bound);
		
		while (i.hasNext() && service == null)
		{
			QName qname = (QName) i.next();
			if (qname.getLocalPart().equals(DEVICE))
			{
				System.out.println("Device: " + qname.getLocalPart());
				Log.info("Connecting...");
				if (gui != null)
				{
					gui.setStatus("connecting...");
				}
				try {
					Device device = devRef.getDevice();
					QNameSet serviceTypes = new QNameSet();
					serviceTypes.add(new QName(SERVICE, NAMESPACE));
					org.ws4d.java.structures.Iterator serviceIterator = device.getServiceReferences(serviceTypes, SecurityKey.EMPTY_KEY);
					DefaultServiceReference serviceRef = (DefaultServiceReference) serviceIterator.next();
					service = serviceRef.getService();
//					System.out.println("ServiceId: " + service.toString());
					
					Log.info("Subscribing to event");
					EventSource eventSource = service.getEventSource(new QName(SERVICE, NAMESPACE), "IndoorTemperatureEvent", null, null);
					
					if (eventSource != null) {
						// add binding
						DataStructure bindings = new org.ws4d.java.structures.ArrayList();
						IPAddress ip = (IPAddress) IPNetworkDetection.getInstance().getAddressesForInterface(false, "wlan0").next();
						Log.info("IP Address for event sink is " + ip.toString());
						Random generator = new Random();
						int port = generator.nextInt(55000) + 10000;
						HTTPBinding binding = new HTTPBinding(ip, port, "/EventSink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
						bindings.add(binding);

						// subscribe
						notificationSub = eventSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
						
						if (gui != null && notificationSub != null)
						{
							Log.info("Connected");
							gui.setStatus("connected");
						}
					}
					else
					{
						Log.info("No events found");
					}
					
					bound = true;
					
					updateTemperature();

				} catch (CommunicationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (AuthorizationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EventingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public void finishedSearching(boolean entityFound, SearchParameter search) {
		if (!entityFound && !requested && gui != null && !bound)
		{
			Log.info("Not connected");
			gui.setStatus("not connected");
		}
		else if ((requested || bound) && gui != null)
		{
			Log.info("Connected");
			gui.setStatus("connected");
		}
		requested = false;
	}
	
	@Override
	public void helloReceived(HelloData helloData)
	{
		Log.info("Hello received");
//		gui.setStatus("connection requested...");
		if (GpioListener.getInstance().areButtonsHighDelayed(DispatchingProperties.getInstance().getResponseWaitTime() / 1000))
		{
//			resetPairing();
			Log.info("Hello accepted");
			Log.info("Connecting...");
			if (gui != null)
			{
				gui.setStatus("connecting...");
			}
			DeviceReference devRef = getDeviceReference(helloData);
			deviceFound(devRef, null);
		}
		else 
		{
			Log.info("Hello rejected");
//			gui.setStatus("not connected");
		}
	}
	
	@Override
	public void searchKnownDevices()
	{
		Log.info("Searching for devices...");
		if (!requested && gui != null)
		{
			Log.info("Searching...");
			gui.setStatus("searching...");
		}
		SearchParameter search = new SearchParameter();
		QNameSet qnameSet = new QNameSet();
		qnameSet.add(new QName(DEVICE, NAMESPACE));
		search.setDeviceTypes(qnameSet);
		SearchManager.searchDevice(search, this, null);
	}
	
	public void registerKnownHello()
	{
		SearchParameter search = new SearchParameter();
		QNameSet qnameSet = new QNameSet();
		qnameSet.add(new QName(DEVICE, NAMESPACE));
		search.setDeviceTypes(qnameSet);
		this.registerHelloListening(search);
	}
	
	@Override
	public void resetPairing()
	{
		Log.info("Resetting pairing");
		bound = false;
		unsubscribeFromAllEvents();
		if (gui != null)
		{
			gui.setStatus("reseted");
		}
		service = null;
	}
	
	public void updateTemperature()
	{
		// we are looking for an Operation by its name
		Operation op = service.getOperation(null, "GetCurrentTemperature", null, null);
		ParameterValue input = op.createInputValue();
		ParameterValue result;

		try {
			result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			Log.info("Response from the GetCurrentTemperature: " + result.toString());
			if (gui != null && bound)
			{
				Log.info("Updating Temperature");
				String valueString = ParameterValueManagement.getString(result, "value");
				String unit = ParameterValueManagement.getString(result, "unit");
				int value = Integer.parseInt(valueString);
				this.temperature = value;
				this.unit = unit;
				gui.setIndoorTemp(value, unit);
			}
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendTemperature()
	{
		// we are looking for an Operation by its name
		Operation op = service.getOperation(null, "SetDesiredTemperature", null, null);
		ParameterValue input = op.createInputValue();
		ParameterValue result;
		
		ParameterValueManagement.setString(input, "value", "" + temperature);
		ParameterValueManagement.setString(input, "unit", unit);

		try {
			result = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
			System.out.println("Response from the SetCurrentTemperature: " + result.toString());
		} catch (AuthorizationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setGui(MainWindow gui)
	{
		this.gui = gui;
	}
	
	@Override
	public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue)
	{
		Log.info("Notification event received " + parameterValue.toString());
		if (gui != null && bound)
		{
			String valueString = ParameterValueManagement.getString(parameterValue, "value");
			String unit = ParameterValueManagement.getString(parameterValue, "unit");
			int value = Integer.parseInt(valueString);
			this.temperature = value;
			this.unit = unit;
			gui.setIndoorTemp(value, unit);
		}
		
		return null;

	}
	
	public void unsubscribeFromAllEvents()
	{
		try {
			System.out.println("trying to unsubscribe");
			if (notificationSub != null)
			{
				System.out.println("unsubscribing...");
				notificationSub.unsubscribe();
				notificationSub = null;
			}
			System.out.println("successfully unsubscribed");
		} catch (EventingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void requestReceived()
	{
		requested = true;
		Log.info("Connection requested...");
		if (gui != null && !bound)
		{
			gui.setStatus("connection requested...");
		}
	}
	
	@Override
	public void requestAccepted()
	{
//		requested = false;
		bound = true;
		Log.info("Connected...");
		if (gui != null)
		{
			gui.setStatus("connected");
		}
	}
	
	@Override
	public void requestRejected()
	{
		requested = false;
		Log.info("New connection rejected");
		if (gui != null && !bound)
		{
			Log.info("Not connected...");
			gui.setStatus("not connected");
		}
		else if (gui != null)
		{
			Log.info("connected...");
			gui.setStatus("connected");
		}
	}
	
	@Override
	public void buttonPressed(int pinNumber)
	{
		if (pinNumber == 0)
		{
			temperature--;		
		}
		if (pinNumber == 1)
		{
			temperature++;
		}
		
		if (gui != null)
		{
			gui.setIndoorTemp(temperature, unit);
		}
		
//		gui.setStatus("Updating button " + pinNumber + ";temp:" + temp);
		if (bound)
		{
			sendTemperature();
		}
		
	}
	
//	public static void main(String[] args)
//	{
//		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
//		DPWSProperties properties = DPWSProperties.getInstance();
//		properties.addSupportedDPWSVersion(new DPWSProtocolVersion(DPWSConstants2006.DPWS_VERSION2006));
//
//		JMEDSFramework.start(args);
//
//		AirCondClient client = new AirCondClient();
//		client.searchKnownDevices();
//
//	}

}
