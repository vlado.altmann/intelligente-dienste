package org.uni.energymeter.sml.client;

import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.uni.energymeter.sml.device.SMLEnergyMeterDevice;
import org.ws4d.java.DPWSFramework;
import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.client.SearchParameter;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.communication.protocol.soap.generator.DPWSEXI;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.configuration.SecurityProperties;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.ServiceCommons.PortType;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLRepository;
import org.ws4d.java.service.parameter.StringValue;

import com.siemens.ct.exi.EXIFactory;
import com.siemens.ct.exi.EncodingOptions;
import com.siemens.ct.exi.GrammarFactory;
import com.siemens.ct.exi.grammar.Grammar;

public class DPWSClient extends DefaultClient  {
	
	static final String SMART_METER_GATEWAY_SERVICE = "SmartMeterGatewayService";
	static final String SMART_METER_SERVICE = "SMLEnergyMeterService";
	static final String SMART_METER_DEVICE = "SMLEnergyMeter";
	static final String ENERGY_METER = "E-Meter";
	static final String SMART_METER_TYPE = "SMLEnergyMeterInterface";
	static final String SMART_METER_NAMESPACE = "http://www.ws4d.org/sml";
	
	private List<Device> deviceList;
	private List<Service> serviceList;
	
	private DPWSClient()
	{
		System.out.println("Starting Client...");
		deviceList = new LinkedList<Device>();
	}
	
	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search){
		Device device = null;
		try {
			device = devRef.getDevice();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		
		EndpointReference endRef = devRef.getEndpointReference();
		endRef.isXAddress();
		System.out.println("Found Device: " + device.getFriendlyName("en"));
		for (Iterator i = device.getPortTypes(); i.hasNext();)
		{
			if (((QName) i.next()).getLocalPart().equals(SMART_METER_DEVICE))
			{
				for (Iterator it_servRef = device.getServiceReferences(); it_servRef.hasNext();) {
					ServiceReference servRef = (ServiceReference) it_servRef.next();
					Service service = null;
					try {
						service = servRef.getService();
					} catch (TimeoutException e) {
						e.printStackTrace();
					}
					String id = service.getServiceId().toString();
					System.out.println("Service: " + id);

					if (id.equals(SMART_METER_SERVICE)) {
						getProfileListRequest(service);
					}
				}
			}
		}

	}
	
	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
        	System.out.printf("InetAddress: %s\n", inetAddress);
        }
        System.out.printf("\n");
     }
	
	public void getStaticDevice()
	{				
		try {
//			InetAddress thisIp = InetAddress.getLocalHost();
//			System.out.println("IP: " + thisIp.getHostAddress());
//
//			NetworkInterface inet = NetworkInterface.getByInetAddress(thisIp);
//			displayInterfaceInformation(inet);

			DeviceReference devRef = SearchManager.getDeviceReference(new EndpointReference(new URI("urn:uuid:5afda9ab-45d6-41e1-9d64-3e3cdf127836")),
					new URI("http://139.30.201.249:59876/5afda9ab-45d6-41e1-9d64-3e3cdf127836"), null, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

			Device device = devRef.getDevice();
			deviceList.add(device);

			System.out.println("Found Static Device: " + device.getFriendlyName("en"));
//			devicePane.addDevice(device);

		} 
//		catch (UnknownHostException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
//		catch (SocketException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		catch (TimeoutException e) {
			e.printStackTrace();
		}		
	}
	
	public void getDeviceList(){
		QName smsType = new QName(SMART_METER_DEVICE, SMART_METER_NAMESPACE);
//		QName smgType = new QName("SmartMeterGateway", "http://www.ws4d.org/SmartMeterGateway");
		QNameSet types = new QNameSet(smsType);
//		types.add(smgType);
		SearchParameter criteria = new SearchParameter();
		criteria.setDeviceTypes(types);

		this.searchDevice(criteria);
	}
	
	public List<Device> getList()
	{
		return deviceList;
	}
	
	public List<Service> getServiceList(Device device)
	{
		if (device == null) {
			return null;
		}		
		
		if (serviceList == null){
			serviceList = new LinkedList<Service>();
		} else {
			serviceList.clear();
		}
		
		for (Iterator it_servRef = device.getServiceReferences(); it_servRef.hasNext();) {
			ServiceReference servRef = (ServiceReference) it_servRef.next();
			Service service = null;
			try {
				service = servRef.getService();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
			String id = service.getServiceId().toString();
			System.out.println("Service: " + id);
			serviceList.add(service);
			if (id.equals(DPWSClient.SMART_METER_SERVICE)) {
				System.out.println("Found Smart Meter Service");
			} else if (id.equals(DPWSClient.SMART_METER_GATEWAY_SERVICE)) {
				System.out.println("Found Smart Meter Gateway Service");
			} else {
				System.out.println("Found Generic Service");
			}
		}
		return serviceList;		
	}
	
	public String getState(String deviceUUID, String serviceName)
	{
		Service service = null;
		
		if (serviceName == null) 
			return null;
		
		for (Service serviceTmp: serviceList)
		{
			if (serviceTmp.getServiceId().toString().equals(serviceName))
			{
				service = serviceTmp;
				break;
			}
		}
		
		Operation ourTwoWayOperation = service.getAnyOperation(
				new QName(DPWSClient.SMART_METER_TYPE, DPWSClient.SMART_METER_NAMESPACE), "GetState");
		
		ParameterValue returnMessagePV;
		String returnValue = new String("0");
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = ourTwoWayOperation.invoke(null);
			returnValue = returnMessagePV.get("State/StateDescription").toString();
//			serial.append(returnMessagePV.get("SerialNumber").toString());
//			returnValue = stateDescription.toString();
//			System.out.println("Smart Meter returns: " + serial);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public String getActualCount(String deviceUUID, String serviceName)
	{
		Service service = null;
		
		if (serviceName == null) 
			return null;
		
		for (Service serviceTmp: serviceList)
		{
			if (serviceTmp.getServiceId().toString().equals(serviceName))
			{
				service = serviceTmp;
				break;
			}
		}
		
		Operation ourTwoWayOperation = service.getAnyOperation(
				new QName(DPWSClient.SMART_METER_TYPE, DPWSClient.SMART_METER_NAMESPACE), "GetActualCount");
		
		ParameterValue returnMessagePV;
		String returnValue = new String("0");
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = ourTwoWayOperation.invoke(null);
			returnValue = returnMessagePV.get("Count").toString();
//			returnValue = stateDescription.toString();
//			System.out.println("Smart Meter returns: " + returnValue);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public String getConsumption(String deviceUUID, String serviceName)
	{
		Service service = null;
		
		if (serviceName == null) 
			return null;
		
		for (Service serviceTmp: serviceList)
		{
			if (serviceTmp.getServiceId().toString().equals(serviceName))
			{
				service = serviceTmp;
				break;
			}
		}
		
		Operation ourTwoWayOperation = service.getAnyOperation(
				new QName(DPWSClient.SMART_METER_TYPE, DPWSClient.SMART_METER_NAMESPACE), "GetPowerQuality");
		
		ParameterValue returnMessagePV;
		String returnValue = new String("0");
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = ourTwoWayOperation.invoke(null);
			returnValue = returnMessagePV.get("Quality").toString();
//			returnValue = stateDescription.toString();
//			System.out.println("Smart Meter returns: " + returnValue);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public void getProfileListRequest(Service service)
	{
		// get primitive data types
		Type xsString = SchemaUtil.getSchemaType("string");
		
		// create inner elements for MessageType
		Element parameterTreePath = new Element(new QName("parameterTreePath", SMART_METER_NAMESPACE), xsString);

		// create MessageType and add inner elements
		ComplexType SMLGetProfileListReqMessage = new ComplexType(new QName("SMLGetProfileListReqMessage", SMART_METER_NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
		SMLGetProfileListReqMessage.addElement(parameterTreePath);
		
		// create element
		Element SMLGetProfileListReq = new Element(new QName("SMLGetProfileListReq", SMART_METER_NAMESPACE), SMLGetProfileListReqMessage);
		
		// create ParameterValue from element
		ParameterValue messageInstance = ParameterValue.createElementValue(SMLGetProfileListReq);

		//cast and set the value
		StringValue messageValue = (StringValue) messageInstance.get("parameterTreePath");
		// set value for the string
		messageValue.set("01.02.03.04.05.06");
		
		Operation ourTwoWayOperation = service.getAnyOperation(
				new QName(DPWSClient.SMART_METER_TYPE, DPWSClient.SMART_METER_NAMESPACE), "SMLGetProfileList");
		
		ParameterValue returnMessagePV;
		String returnValue = new String("0");
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = ourTwoWayOperation.invoke(messageInstance);
			returnValue = returnMessagePV.get("parameterTreePath").toString();
//			returnValue = stateDescription.toString();
			System.out.println("Smart Meter returns: " + returnValue);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
	
	public String toggleWindow(String deviceUUID, String serviceName, boolean isOpen)
	{
		Service service = null;
		
		if (serviceName == null) 
			return null;
		
		for (Service serviceTmp: serviceList)
		{
			if (serviceTmp.getServiceId().toString().equals(serviceName))
			{
				service = serviceTmp;
				break;
			}
		}
		
		// get primitive data types
		Type xsString = SchemaUtil.getSchemaType("string");
		
		// create inner elements for MessageType
		Element message = new Element(new QName("Message", SMART_METER_NAMESPACE), xsString);
		Element serviceId = new Element(new QName("ServiceId", SMART_METER_NAMESPACE), xsString);

		// create MessageType and add inner elements
		ComplexType messageType = new ComplexType(new QName("MessageType", SMART_METER_NAMESPACE), ComplexType.CONTAINER_SEQUENCE);
		messageType.addElement(message);
		messageType.addElement(serviceId);
		
		// create element
		Element messageElement = new Element(new QName("Message", SMART_METER_NAMESPACE), messageType);
		
		// create ParameterValue from element
		ParameterValue messageInstance = ParameterValue.createElementValue(messageElement);

		//cast and set the value
		StringValue messageValue = (StringValue) messageInstance.get("Message");
		// set value for the string
		if (!isOpen)
		{
			messageValue.set("open window");
		}
		else
		{
			messageValue.set("close window");
		}
		
//		windowToggled = !windowToggled;
		
		StringValue serviceIdValue = (StringValue) messageInstance.get("ServiceId");
		serviceIdValue.set("");
		
		Operation ourTwoWayOperation = service.getAnyOperation(
				new QName(DPWSClient.SMART_METER_TYPE, DPWSClient.SMART_METER_NAMESPACE), "SetMessage");
		
		ParameterValue returnMessagePV;
		String returnValue = new String("0");
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = ourTwoWayOperation.invoke(messageInstance);
			returnValue = returnMessagePV.get("StateDescription").toString();
//			returnValue = stateDescription.toString();
//			System.out.println("Smart Meter returns: " + returnValue);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	
	public static void pressEnterToContinue() {
        System.out.println("Press ENTER to continue...");
        try {
            System.in.read();
            while (System.in.available() > 0)
                System.in.read(); //flush the buffer
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static void main(String[] args) throws Exception {
		
		System.out.println("Working Path: " + System.getProperty("user.dir"));
		
		KeyStore ks = KeyStore.getInstance("JKS");
		
		// get user password and file input stream
	    char[] password = "2425581".toCharArray();

	    java.io.FileInputStream fis = null;
	    try {
	        fis = new java.io.FileInputStream("src/org/uni/energymeter/sml/device/keystore.jks");
	        ks.load(fis, password);
	    } finally {
	        if (fis != null) {
	            fis.close();
	        }
	    }
	    
	    System.out.println("Keystore aliases:");
	    Enumeration<String> aliases = ks.aliases();
        for (String alias : Collections.list(aliases)) {
        	System.out.println(alias);
        }
        
		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		System.setProperty("javax.net.ssl.trustStore", "src/org/uni/energymeter/sml/device/keystore.jks");
		System.setProperty("javax.net.ssl.trustStorePassword", "2425581");
		
		DPWSProperties properties = DPWSProperties.getInstance();
		Property prop;
		
		// Set DPWS Framework Properties for uDPWS
		// Avoid chunked mode
//		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_SERVER_AVOIDCHUNKED,
//				"true");
//		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);
//		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_CLIENT_AVOIDCHUNKED,
//				"true");
//		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);
		// disable keep alive
		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_SERVER_KEEPALIVE,
				"false");
		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);
		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_CLIENT_KEEPALIVE,
				"false");
		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);
		
		// DPWS Version		
//		properties.addSupportedDPWSVersion(DPWSConstants2006.DPWS_VERSION2006);
		properties.removeSupportedDPWSVersion(DPWSConstants.DPWS_VERSION2009);
		
		properties.setEXIParameters(DPWSProperties.PROP_EXI_FOR_INVOKE);
		EXIFactory exiFactory = DPWSEXI.getEXIFactoryInstance();
		exiFactory.getEncodingOptions().setOption(EncodingOptions.INCLUDE_COOKIE);
		GrammarFactory grammarFactory = GrammarFactory.newInstance();
		Grammar g = grammarFactory.createGrammar("src/org/uni/energymeter/sml/device/SMLEnergyMeter.xsd");
		exiFactory.setGrammar(g);
		
		properties.setTransportParameters(DPWSProperties.PROP_TRANSPORT_COAP);
		
		properties.setMessageIDParameters(DPWSProperties.PROP_MESSAGE_SHORTID);
		properties.setMessageIDLength(2);
		
		properties.useDefaultDestination(true);
		
		// Start DPWS Framework
		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSFramework.start(args);

		// load WSDLs (there's a bug that the sensor can not answer both
		// GetMetadataRequest at the same time)
		WSDLRepository wsdlRepo = WSDLRepository.getInstance();
		
		DPWSClient client = new DPWSClient();
//		DPWSClient dpwsClient = new DPWSClient();
		client.getDeviceList();
	}
}
