package org.uni.energymeter.sml.device;

import java.io.IOException;

import org.ws4d.java.constants.MEXConstants;
import org.ws4d.java.constants.SOAPConstants;
import org.ws4d.java.io.xml.ElementHandler;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.io.xml.XmlSerializer;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.CustomizeMDataHandler;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.SerializeUtil;
import org.ws4d.java.util.WS4DIllegalStateException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class WindowsExtensionHandler implements ElementHandler{

	private static String						NAMESPACE	= "http://schemas.microsoft.com/windows/pnpx/2005/10";
	
	private static String 						NAMESPACE_PREFIX = "PNPX";
	
	private static final WindowsExtensionHandler	INSTANCE	= new WindowsExtensionHandler();

	private WindowsExtensionHandler() {
		super();
	}
	
	@Override
	public Object handleElement(QName elementName, ElementParser parser)
			throws XmlPullParserException, IOException {
		
		String content = "";
		int event;
		if (parser.getNamespace().equals(NAMESPACE)) {
			String name = parser.getName();
			content = content + name + "\n[\n";
			parser.nextTag();
			do {
				content = content + parser.getName() + " = ";
				parser.next();
				content = content + parser.getText() + ";\n";
				parser.next();
				event = parser.nextTag();
			} while (event != XmlPullParser.END_TAG);
			content = content + "]";
		}
		return content;
	}

	@Override
	public void serializeElement(XmlSerializer serializer, QName qname,
			Object data) throws IllegalArgumentException,
			WS4DIllegalStateException, IOException {
		
		serializer.setPrefix(NAMESPACE_PREFIX, NAMESPACE);
//		boolean ok = true;
//		if (serializer.getPrefix(NAMESPACE, ok) != NAMESPACE_PREFIX)
//		{
//			serializer.setPrefix(NAMESPACE_PREFIX, NAMESPACE);
//		}
//		String ns = serializer.getPrefix("http://schemas.microsoft.com/windows/pnpx/2005/10", ok);
		serializer.startTag(NAMESPACE, qname.getLocalPart());
		serializer.text((String) data);
		serializer.endTag(NAMESPACE, qname.getLocalPart());
		
	}

	/**
	 * Give a static instance of the  WindowsExtensionHandler
	 * @return
	 */
	public static WindowsExtensionHandler getInstance() {
		return INSTANCE;
	}
	
}
