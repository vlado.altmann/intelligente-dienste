package org.uni.energymeter.sml.device;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.KeyStore;
import java.util.Collections;
import java.util.Enumeration;

import org.ws4d.java.DPWSFramework;
import org.ws4d.java.communication.HTTPBinding;
import org.ws4d.java.communication.HTTPSBinding;
import org.ws4d.java.communication.connection.ip.IPAddress;
import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.protocol.soap.generator.DPWSEXI;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.configuration.SecurityProperties;
import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.InvokeDelegate;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationStub;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.siemens.ct.exi.EXIFactory;
import com.siemens.ct.exi.EncodingOptions;
import com.siemens.ct.exi.GrammarFactory;
import com.siemens.ct.exi.grammar.Grammar;

import sun.net.util.IPAddressUtil;

public class SMLEnergyMeterDevice {
	// TODO enter your local IP here
	private static final IPAddress	IP	= IPNetworkDetection.getInstance().getIPAddress("localhost");
	static final String ENERGY_METER = "SMLEnergyMeter";
	static final String ENERGY_METER_SERVICE = "SMLEnergyMeterService";
	static final String TYPE_ENERGY_METER = "SMLEnergyMeterInterface";
	static final String NAMESPACE_ENERGY_METER = "http://www.ws4d.org/sml";
	static final boolean isSecure = false;
	static final int port = 50005;
	
	//Measured Data 
	private String data = new String();
	private String dateTime = new String();
	private String activePower = new String();
	private String apparentPower = new String();
	private String reactivePower = new String();
	private String voltage = new String();
	private String current = new String();
	private String powerFactor = new String();
	private String activeEnergy = new String();
	private String apparentEnergy = new String();
	private String reactiveEnergy = new String();
	private String load = new String();
	
	public SMLEnergyMeterDevice(String[] args) throws Exception
	{
		System.out.println("Working Path: " + System.getProperty("user.dir"));
		
		KeyStore ks = KeyStore.getInstance("JKS");
		
		// get user password and file input stream
	    char[] password = "2425581".toCharArray();

	    java.io.FileInputStream fis = null;
	    try {
	        fis = new java.io.FileInputStream("src/org/uni/energymeter/sml/device/keystore.jks");
	        ks.load(fis, password);
	    } finally {
	        if (fis != null) {
	            fis.close();
	        }
	    }
	    
	    System.out.println("Keystore aliases:");
	    Enumeration<String> aliases = ks.aliases();
        for (String alias : Collections.list(aliases)) {
        	System.out.println(alias);
        }
		
		String os = System.getProperty("os.name").toLowerCase();
		
		// always start the framework first
//		System.out.println("Working Dir: " + System.getProperty("user.dir"));
		
		DPWSProperties properties = DPWSProperties.getInstance();
		Property prop;
		
		// disable keep alive
		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_SERVER_KEEPALIVE,
				"false");
		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);
		
		prop = new Property(DPWSProperties.PROP_DPWS_HTTP_CLIENT_KEEPALIVE,
				"false");
		properties.setProperties(Properties.HEADER_SECTION_DPWS, prop);

		properties.setHTTPResponseChunkedMode(DPWSProperties.HTTP_CHUNKED_OFF);
		properties.setHTTPRequestChunkedMode(DPWSProperties.HTTP_CHUNKED_OFF);
		
		// DPWS Version
//		properties.addSupportedDPWSVersion(DPWSConstants2006.DPWS_VERSION2006);
		properties.removeSupportedDPWSVersion(DPWSConstants.DPWS_VERSION2009);
		
		SecurityProperties secProperties = SecurityProperties.getInstance();
		secProperties.setKeyStoreFileName("src/org/uni/energymeter/sml/device/keystore.jks");
		secProperties.setKeyStorePaswd("2425581");
		
		properties.setEXIParameters(DPWSProperties.PROP_EXI_FOR_INVOKE);
		EXIFactory exiFactory = DPWSEXI.getEXIFactoryInstance();
		exiFactory.getEncodingOptions().setOption(EncodingOptions.INCLUDE_COOKIE);
		GrammarFactory grammarFactory = GrammarFactory.newInstance();
		Grammar g = grammarFactory.createGrammar("src/org/uni/energymeter/sml/device/SMLEnergyMeter.xsd");
		exiFactory.setGrammar(g);
		
		properties.setTransportParameters(DPWSProperties.PROP_TRANSPORT_COAP);
		
		properties.setMessageIDParameters(DPWSProperties.PROP_MESSAGE_SHORTID);
		properties.setMessageIDLength(2);
		
		properties.useDefaultDestination(true);
		
		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
//		DPWSFramework.start(new String[] { "src/org/uni/smartmeter/device/test.properties" });
		DPWSFramework.start(args);
		
		// create a simple device ...
		DefaultDevice device = new DefaultDevice();
		
		NetworkInterface inet;
		
		System.out.println("OS: " + os);
		
		if (os.contains("win"))
    	{
			inet = NetworkInterface.getByName("eth3"); //Windows
    	}
    	else if (os.contains("mac"))
    	{
    		inet = NetworkInterface.getByName("en1"); //MAC
    	}
    	else
    	{
    		inet = NetworkInterface.getByName("eth0"); //Linux
    	}
		
//		displayInterfaceInformation(inet);
		InetAddress thisIp = InetAddress.getLocalHost();		
		Enumeration<InetAddress> inetAddresses = inet.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
        	if (IPAddressUtil.isIPv4LiteralAddress(inetAddress.getHostAddress()))
        	{
        		thisIp = inetAddress;
        	}
        }
        System.out.println("IP Address: " + thisIp.getHostAddress());
		
		// ... add a binding the device can be accessed over ...
        if (isSecure)
        {
        	HTTPSBinding deviceBinding = new HTTPSBinding(new IPAddress(thisIp.getHostAddress()), port, "/smldev");
        	deviceBinding.setAlias("dpwssslcert");
        	//	device.setSecureDevice();
        	device.addBinding(deviceBinding);
        }
        else
        {
        	HTTPBinding deviceBinding = new HTTPBinding(new IPAddress(thisIp.getHostAddress()), port, "/smldev");
        	device.addBinding(deviceBinding);
        }
        		
		//Metadata
		device.addManufacturer("en", "University of Rostock");
		device.addManufacturer("de", "Universitaet Rostock");
		device.addFriendlyName("en", "SMLEnergyMeter");
		device.addFriendlyName("de", "SMLEnergyMeter");
		device.addModelName("en", "SMLEnergyMeter");
		device.addModelName("de", "SMLEnergyMeter");
		device.setFirmwareVersion("Version 0.1");
		device.setManufacturerUrl("http://www.uni-rostock.de");
		device.setModelNumber("0.2");
		device.setModelUrl("http://www.uni-rostock.de");
		device.setPresentationUrl("http://www.imd.uni-rostock.de/ma/va015/smartmetering/demo.html");
		device.setSerialNumber("00001");
		
		QName smsType = new QName(ENERGY_METER, NAMESPACE_ENERGY_METER);
		QNameSet types = new QNameSet(smsType);		
		device.getDiscoveryData().addTypes(types);		
		
		ElementHandlerRegistry elementRegistry = ElementHandlerRegistry.getRegistry();
		WindowsExtensionHandler myHandler = WindowsExtensionHandler.getInstance(); 
		elementRegistry.registerElementHandler(new QName("DeviceCategory"), myHandler);
		String winExt = new String("HomeAutomation");
		device.getModelMetadata().addUnknownElement(new QName("DeviceCategory"), winExt);
		//End of Metadata

		// ... and a service
		DefaultService service = new DefaultService();

		/*
		 * =====================================================================
		 * DEFINE THE SERVICE! this code loads the WSDL from the specified URI
		 * and adds all supported port types and their contained operations to
		 * the service. No need to code the input/output elements of the
		 * operations and their types manually!
		 * =====================================================================
		 */
		service.define(new URI("local:/org/uni/energymeter/sml/device/SMLEnergyMeter.wsdl"));
		service.setServiceId(new URI(ENERGY_METER_SERVICE));

		//GetData
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) service.getOperation("GPLQ");
		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invoke(Operation operation, ParameterValue request) throws InvocationException {
				// extract the expected input args ...
				ParameterValue val = request.get("parameterTreePath[0]");
				System.out.println(val.toString());

				// create suitable response ...
				ParameterValue response = operation.createOutputValue();
				((StringValue) response.get("actTime")).set("2012-01-09T09:00:00");
				((StringValue) response.get("regPeriod")).set("150");
				((StringValue) response.get("parameterTreePath[0]")).set("01.02.03.04.05.06");				
				((StringValue) response.get("period[0]/objName")).set("02.82.21.89.63.25");
				((StringValue) response.get("period[0]/unit")).set("W");
				((StringValue) response.get("period[0]/scaler")).set("1");
				((StringValue) response.get("period[0]/value")).set("1029");
				((StringValue) response.get("valTime")).set("2012-01-09T09:00:00");
				((StringValue) response.get("status")).set("200");
				// ... and send it back
				return response;
			}

		});
		
		
		/*
		 * this is the binding for the service, i.e. the address it will accept
		 * messages over
		 */
		if (isSecure)
		{
			
			HTTPSBinding serviceBinding = new HTTPSBinding(new IPAddress(thisIp.getHostAddress()), port, "/smlsvc");
			serviceBinding.setAlias("dpwssslcert");
			//		service.setSecureService();
			service.addBinding(serviceBinding);
		}
		else
		{
			HTTPBinding serviceBinding = new HTTPBinding(new IPAddress(thisIp.getHostAddress()), port, "/smlsvc");
			service.addBinding(serviceBinding);
		}


		// add service to device in order to support automatic discovery ...
		device.addService(service);
		/*
		 * ... and start the device; this also starts all services located on
		 * top of that device
		 */
		device.start();
	}
		/**
		 * @param args
		 * @throws Exception
		 */
		public static void main(String[] args) throws Exception {
			
			SMLEnergyMeterDevice energyMeterDevice = new SMLEnergyMeterDevice(args);
//			DPWSClient dpwsClient = new DPWSClient();
//			dpwsClient.getDeviceList();
		}
		
		static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
	        System.out.printf("Display name: %s\n", netint.getDisplayName());
	        System.out.printf("Name: %s\n", netint.getName());
	        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
	        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
	        	System.out.printf("InetAddress: %s\n", inetAddress.getHostAddress());
	        }
	        System.out.printf("\n");
	     }
}
