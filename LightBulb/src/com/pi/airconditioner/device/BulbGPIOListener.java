package com.pi.airconditioner.device;

import org.ws4d.java.pi.gpio.GpioListener;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class BulbGPIOListener {
	// create gpio controller instance
	final GpioController gpio = GpioFactory.getInstance();
	// provision gpio pins #04 as an output pin and make sure is is set to LOW at startup
	GpioPinDigitalOutput bulb = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00,   // PIN NUMBER
	                                                           "Bulb",           // PIN FRIENDLY NAME (optional)
	                                                           PinState.LOW);      // PIN STARTUP STATE (optional)
	
	static BulbGPIOListener instance; 
	
	// Suppress default constructor for noninstantiability
    private BulbGPIOListener() {}
    
    public static synchronized BulbGPIOListener getInstance() {
    	if (BulbGPIOListener.instance == null) {
    		BulbGPIOListener.instance = new BulbGPIOListener();
    	}
    	return BulbGPIOListener.instance;
    }
	
	public void setBulbStateOn()
	{
		bulb.high();
		System.out.println("Turning On");
	}

	public void setBulbStateOff()
	{
		bulb.low();
		System.out.println("Turning Off");
	}
	
	public void pulse(int millis)
	{
		bulb.pulse(millis);
	}
}
