package com.pi.airconditioner.device;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class SwitchOperation extends Operation {

	private GetStateOperation getOperation;

	public SwitchOperation() {
		super("Switch", new QName(LightBulbService.SERVICE_TYPE, LightBulbDevice.NAMESPACE));

		// create element of type TemperatureType
		Element switchElement = new Element(new QName("switch", LightBulbDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set the input of the operation
		setInput(switchElement);

		// create reply element
		Element reply = new Element(new QName("status", LightBulbDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set this element as output
		setOutput(reply);
		
	}

	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		// get string value from input
		String value = ParameterValueManagement.getString(parameterValue, "switch");
		
		// create output and set value
		ParameterValue result = createOutputValue();
		
		if (value.compareTo("on") == 0)
		{
			BulbGPIOListener.getInstance().setBulbStateOn();
			getOperation.setState(value);
			ParameterValueManagement.setString(result, "status", value);
		}
		else if (value.compareTo("off") == 0)
		{
			BulbGPIOListener.getInstance().setBulbStateOff();
			getOperation.setState(value);
			ParameterValueManagement.setString(result, "status", value);
		}
		else
		{
			ParameterValueManagement.setString(result, "status", "error");
		}

		return result;
	}
	
	public void setGetStateOperation(GetStateOperation op)
	{
		getOperation = op;
	}
}
