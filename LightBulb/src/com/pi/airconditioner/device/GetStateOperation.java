package com.pi.airconditioner.device;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class GetStateOperation extends Operation {

	private String state = "off";

	public GetStateOperation() {
		super("GetCurrentState", new QName(LightBulbService.SERVICE_TYPE, LightBulbDevice.NAMESPACE));

		// create element of type TemperatureType
		Element stateElement = new Element(new QName("state", LightBulbDevice.NAMESPACE), SchemaUtil.TYPE_STRING);

		// set the input of the operation
//		setInput(null);

		// set this element as output
		setOutput(stateElement);
	}

	public ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException {

		// create output and set value
		ParameterValue result = createOutputValue();
		ParameterValueManagement.setString(result, "state", "" + state);

		return result;
	}
	
	public void setState(String value)
	{
		state = value;
	}

}
