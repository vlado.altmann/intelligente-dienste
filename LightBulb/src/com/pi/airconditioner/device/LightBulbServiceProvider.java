package com.pi.airconditioner.device;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
import org.ws4d.java.pi.gpio.GpioListener;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.SearchParameter;
import org.ws4d.java.util.Log;

import com.pi.airconditioner.client.LightBulbClient;

public class LightBulbServiceProvider {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
		properties.setDiscoveryButton0(1);
//		properties.setDiscoveryButton1(3);
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
//		properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);
		DispatchingProperties.getInstance().setResponseWaitTime(10000);
		
		// mandatory: Starting the DPWS Framework.
		JMEDSFramework.start(args);
		
		// First we need a device.
		LightBulbDevice device = new LightBulbDevice();
		LightBulbClient client = new LightBulbClient();

		// Then we create a service.
		final LightBulbService service = new LightBulbService();

		// In the end we add our service to the device.
		device.addService(service);

		// Do not forget to start the device!
		try {
			device.start();
//			Thread.sleep(5000);
//			client.resetPairing();
//			client.searchKnownDevices();
			client.registerKnownHello();
			GpioListener.getInstance().registerDevice(device);
			GpioListener.getInstance().registerClient(client);
			GpioListener.getInstance().activateHello(true);
			GpioListener.getInstance().activateProbe(true);
			BulbGPIOListener.getInstance().pulse(500);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
