package com.pi.airconditioner.device;

import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;

/**
 * Implementation of the service described in "An Introduction to WS4D and to
 * JMEDS, a framework for distributed communication between devices in a domotic
 * environment" by Pierre-Alexandre Gagn�
 * 
 * @author ajordan
 */
public class LightBulbService extends DefaultService {

	public final static URI	SERVICE_ID	= new URI("LightBulbService");
	public final static String SERVICE_TYPE = "LightBulbInterface";

	/**
	 * Standard Constructor
	 */
	public LightBulbService() {
		super();

		this.setServiceId(SERVICE_ID);

		// add Operations to the service		
		GetStateOperation getState = new GetStateOperation();
		addOperation(getState);

		SwitchOperation switchOp = new SwitchOperation();
		addOperation(switchOp);
		
		switchOp.setGetStateOperation(getState);

	}

}
