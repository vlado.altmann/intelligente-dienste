package org.uni.energymeter.device;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
//import java.io.OutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
//import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;


//import org.ws4d.java.DPWSFramework;
import org.ws4d.java.JMEDSFramework;
//import org.ws4d.java.communication.HTTPBinding;
import org.ws4d.java.communication.protocol.http.HTTPBinding;
import org.ws4d.java.communication.connection.ip.IPAddress;
//import org.ws4d.java.communication.connection.ip.IPNetworkDetection;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.constants.DPWS2006.DPWSConstants2006;
//import org.ws4d.java.constants.DPWSConstants;
import org.ws4d.java.constants.DPWS2009.DPWSConstants2009;
//import org.ws4d.java.constants.DPWS2011.DPWSConstants2011;
//import org.ws4d.java.constants.FrameworkConstants;
import org.ws4d.java.io.xml.ElementHandlerRegistry;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.InvokeDelegate;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationStub;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.security.CredentialInfo;



import sun.net.util.IPAddressUtil;

public class EnergyMeterDevice {
	static final String ENERGY_METER = "EnergyMeter";
	static final String ENERGY_METER_SERVICE = "EnergyMeterService";
	static final String TYPE_ENERGY_METER = "EnergyMeterInterface";
	static final String NAMESPACE_ENERGY_METER = "http://www.ws4d.org/SmartMeter/EnergyMeter";
	static final int port = 50001;
	
	//Measured Data 
	private String data = new String();
	private String dateTime = new String();
	private String activePower = new String();
	private String apparentPower = new String();
	private String reactivePower = new String();
	private String voltage = new String();
	private String current = new String();
	private String powerFactor = new String();
	private String activeEnergy = new String();
	private String apparentEnergy = new String();
	private String reactiveEnergy = new String();
	private String load = new String();

	
	public EnergyMeterDevice(String[] args) throws Exception
	{
		listPorts();
		defaultData();
		String os = System.getProperty("os.name").toLowerCase();
		
        try
        {
        	System.out.println(os);
        	
        	if (os.contains("win"))
        	{
        		connectSerialPort("COM6"); //Windows
        	}
        	else
        	{
        		connectSerialPort("/dev/ttyUSB0"); //Linux
        	}
        }
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		// always start the framework first
//		System.out.println("Working Dir: " + System.getProperty("user.dir"));
		
		DPWSProperties properties = DPWSProperties.getInstance();
		Property prop;
		
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
		properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);		
		
		Log.setLogLevel(Log.DEBUG_LEVEL_ERROR);
//		DPWSFramework.start(new String[] { "src/org/uni/smartmeter/device/test.properties" });
		//DPWSFramework.start(args);
		JMEDSFramework.start(args);
		
		// create a simple device ...
		DefaultDevice device = new DefaultDevice();
		
//		NetworkInterface inet;
//		
//		if (os.contains("win"))
//    	{
//			inet = NetworkInterface.getByName("eth1"); //Windows
//    	}
//    	else
//    	{
//    		inet = NetworkInterface.getByName("wlan0"); //Linux
//    	}
//		
////		displayInterfaceInformation(inet);
//		InetAddress thisIp = InetAddress.getLocalHost();		
//		Enumeration<InetAddress> inetAddresses = inet.getInetAddresses();
//        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
//        	if (IPAddressUtil.isIPv4LiteralAddress(inetAddress.getHostAddress()))
//        	{
//        		thisIp = inetAddress;
//        	}
//        }
//        System.out.println("IP Address: " + thisIp.getHostAddress());
		
		// ... add a binding the device can be accessed over ...
		//device.addBinding(new HTTPBinding(new IPAddress(thisIp.getHostAddress()), port, "/smartmeter/energymeter"));
//		device.addBinding(new HTTPBinding(IPAddress.getLocalIPAddress(thisIp.getHostAddress()), port, "/smartmeter/energymeter", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID ));
		
		//Metadata
		device.addManufacturer("en", "University of Rostock");
		device.addManufacturer("de", "Universitaet Rostock");
		device.addFriendlyName("en", "EnergyMeter");
		device.addFriendlyName("de", "EnergyMeter");
		device.addModelName("en", "EnergyMeter");
		device.addModelName("de", "EnergyMeter");
		device.setFirmwareVersion("Version 0.1");
		device.setManufacturerUrl("http://www.uni-rostock.de");
		device.setModelNumber("0.2");
		device.setModelUrl("http://www.uni-rostock.de");
		device.setPresentationUrl("http://www.imd.uni-rostock.de/ma/va015/smartmetering/demo.html");
		device.setSerialNumber("16435");
		
//		QName smsType = new QName(ENERGY_METER, NAMESPACE_ENERGY_METER);
//		QNameSet types = new QNameSet(smsType);		
//		device.getDiscoveryData().addTypes(types);

		// set PortType
		device.setPortTypes(new QNameSet(new QName(ENERGY_METER, NAMESPACE_ENERGY_METER)));
		
		ElementHandlerRegistry elementRegistry = ElementHandlerRegistry.getRegistry();
		WindowsExtensionHandler myHandler = WindowsExtensionHandler.getInstance(); 
		elementRegistry.registerElementHandler(new QName("DeviceCategory"), myHandler);
		String winExt = new String("HomeAutomation");
		device.getModelMetadata().addUnknownElement(new QName("DeviceCategory"), winExt);
		//End of Metadata

		// ... and a service
		DefaultService service = new DefaultService();

		/*
		 * =====================================================================
		 * DEFINE THE SERVICE! this code loads the WSDL from the specified URI
		 * and adds all supported port types and their contained operations to
		 * the service. No need to code the input/output elements of the
		 * operations and their types manually!
		 * =====================================================================
		 */
		service.define(new URI("local:/org/uni/energymeter/device/EnergyMeter.wsdl"), CredentialInfo . EMPTY_CREDENTIAL_INFO);
		service.setServiceId(new URI(ENERGY_METER_SERVICE));

		//GetData
		// get a reference to a certain operation given its input action URI ...
		OperationStub operation = (OperationStub) service.getOperation(null, "GetData", null, null);
		//OperationStub operation = (OperationStub) service.getOperation(null, "GetData", null, null);
		// ... and specify its business logic by means of an InvokeDelegate
		operation.setDelegate(new InvokeDelegate() {

			/*
			 * (non-Javadoc)
			 * @see
			 * org.ws4d.java.service.InvokeDelegate#invoke(org.ws4d.java.service
			 * .Operation, org.ws4d.java.service.parameter.ParameterValue)
			 */
			public ParameterValue invokeImpl(Operation operation, ParameterValue request, CredentialInfo credentialInfo) throws InvocationException {
				// extract the expected input args ...

				// create suitable response ...
				ParameterValue response = operation.createOutputValue();
				((StringValue) response.get("DateTime")).set(dateTime);
				((StringValue) response.get("ActivePower")).set(activePower);
				((StringValue) response.get("ApparentPower")).set(apparentPower);
				((StringValue) response.get("ReactivePower")).set(reactivePower);
				((StringValue) response.get("Voltage")).set(voltage);
				((StringValue) response.get("Current")).set(current);
				((StringValue) response.get("PowerFactor")).set(powerFactor);
				((StringValue) response.get("ActiveEnergy")).set(activeEnergy);
				((StringValue) response.get("ApparentEnergy")).set(apparentEnergy);
				((StringValue) response.get("ReactiveEnergy")).set(reactiveEnergy);
				((StringValue) response.get("Load")).set(load);
				// ... and send it back
				return response;
			}

		});
		
		
		/*
		 * this is the binding for the service, i.e. the address it will accept
		 * messages over
		 */
//		service.addBinding(new HTTPBinding(IPAddress.getLocalIPAddress(thisIp.getHostAddress()), port, "/smartmeter/energymeterservice", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID));

		// add service to device in order to support automatic discovery ...
		device.addService(service);
		/*
		 * ... and start the device; this also starts all services located on
		 * top of that device
		 */
		device.start();
	}
	
	public void connectSerialPort ( String portName ) throws Exception
    {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() )
        {
            System.out.println("Error: Port is currently in use");
        }
        else
        {
        	
            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
            
            if ( commPort instanceof SerialPort )
            {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(9600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                
                InputStream in = serialPort.getInputStream();
                
                (new Thread(new SerialReader(in))).start();

            }
            else
            {
                System.out.println("Error: Only serial ports are handled");
            }
        }     
    }
    
    /** */
    public class SerialReader implements Runnable 
    {
        InputStream in;
        
        public SerialReader ( InputStream in )
        {
            this.in = in;
        }
        
        public void run ()
        {
            byte[] buffer = new byte[1024];
            int len = -1;
            try
            {
                while ( ( len = this.in.read(buffer)) > -1 )
                {
                	String str = new String(buffer,0,len);
                	
                	if (str.contains("\n"))
                	{
                		data += str.substring(0, str.indexOf("\n") + 1);
                		parseData();
                		data = str.substring(str.indexOf("\n") + 1);
                	}
                	else
                	{
                		data += str;
                	}
                }
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }            
        }
    }
    
    static void listPorts()
    {
        java.util.Enumeration<CommPortIdentifier> portEnum = CommPortIdentifier.getPortIdentifiers();
        while ( portEnum.hasMoreElements() ) 
        {
            CommPortIdentifier portIdentifier = portEnum.nextElement();
            System.out.println(portIdentifier.getName()  +  " - " +  getPortTypeName(portIdentifier.getPortType()) );
        }        
    }
    
    static String getPortTypeName ( int portType )
    {
        switch ( portType )
        {
            case CommPortIdentifier.PORT_I2C:
                return "I2C";
            case CommPortIdentifier.PORT_PARALLEL:
                return "Parallel";
            case CommPortIdentifier.PORT_RAW:
                return "Raw";
            case CommPortIdentifier.PORT_RS485:
                return "RS485";
            case CommPortIdentifier.PORT_SERIAL:
                return "Serial";
            default:
                return "unknown type";
        }
    }
    
    private void defaultData()
    {
    	dateTime = "1.1.1970 00:00";
    	activePower = "0";
    	apparentPower = "0";
    	reactivePower = "0";
    	voltage = "0";
    	current = "0";
    	powerFactor = "0";
    	activeEnergy = "0";
    	apparentEnergy = "0";
    	reactiveEnergy = "0";
    	load = "no load";
    }
    
    private void parseData()
    {
 	   	String[] dataPart = data.split(";");

 	   	if(!(dataPart.length < 11)){
		    	dateTime = dataPart[0];
		    	activePower = dataPart[1];
		    	apparentPower = dataPart[2];
		    	reactivePower = dataPart[3];
		    	voltage = dataPart[4];
		    	current = dataPart[5];
		    	powerFactor = dataPart[6];
		    	activeEnergy = dataPart[7];
		    	apparentEnergy = dataPart[8];
		    	reactiveEnergy = dataPart[9];
		    	if (dataPart[10].equals("0"))
		    	{
		    		load = new String("resistive");
		    	}
		    	else if (dataPart[10].equals("1"))
		    	{
		    		load = new String("inductive");
		    	}
		    	else if (dataPart[10].equals("2"))
		    	{
		    		load = new String("capacitive");
		    	}
		    	else
		    	{
		    		load = new String("unknown");
		    	}
    	
//		    	System.out.println(dataPart[0]	+ "\r\n"
//								+ "activePower " + activePower + "\r\n"
//								+ "apparentPower " + apparentPower + "\r\n"
//								+ "reactivePower"  + reactivePower + "\r\n"
//								+ "voltage " + voltage + "\r\n"
//								+ "current " + current + "\r\n"
//								+ "powerFactor " + powerFactor + "\r\n"
//								+ "activeEnergy " + activeEnergy + "\r\n"
//								+ "apparentEnergy " + apparentEnergy + "\r\n"
//								+ "reactiveEnergy " + reactiveEnergy + "\r\n"
//								+ "load " + load + "\r\n" + "\r\n"
//		    					);
 	   	}else{
 	   		System.out.println("Incomplete Stream!\r\n");
 	   	}

    }
	
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		EnergyMeterDevice energyMeterDevice = new EnergyMeterDevice(args);
	}
	
	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
        	System.out.printf("InetAddress: %s\n", inetAddress.getHostAddress());
        }
        System.out.printf("\n");
     }
}
