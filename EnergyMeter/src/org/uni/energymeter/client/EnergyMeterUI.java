/**
 * 
 */
package org.uni.energymeter.client;

import java.util.Collections;
import java.util.LinkedList;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.AbsolutePanel;

/**
 * @author va015
 *
 */
public class EnergyMeterUI extends Composite {

	private static EnergyMeterUIUiBinder uiBinder = GWT
			.create(EnergyMeterUIUiBinder.class);

	private EnergyMeterServiceAsync smartMeterService = GWT.create(EnergyMeterService.class);
	
	private Canvas valuePlot;
	private int plotWidth = 285;
	private int plotHeight = 120;
	private LinkedList<Integer> values = new LinkedList<Integer>();
	private int maxValue = 1;
	private int minValue = 0;
	private final int REFRESH_RATE = 1;
	static final String upgradeMessage = "Your browser does not support the HTML5 Canvas. Please upgrade your browser to view this demo.";
	
	@UiField Label heading;
	@UiField Label dateTimeLabel;
	@UiField Label activePowerLabel;
	@UiField Label apparentPowerLabel;
	@UiField Label reactivePowerLabel;
	@UiField Label voltageLabel;
	@UiField Label currentLabel;
	@UiField Label powerFactorLabel;
	@UiField Label activeEnergyLabel;
	@UiField Label apparentEnergyLabel;
	@UiField Label reactiveEnergyLabel;
	@UiField Label loadTypeLabel;
	@UiField Label idLabel;
	@UiField AbsolutePanel mainPanel;

	interface EnergyMeterUIUiBinder extends UiBinder<Widget, EnergyMeterUI> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public EnergyMeterUI() {
		initWidget(uiBinder.createAndBindUi(this));
		
		valuePlot = Canvas.createIfSupported();
		valuePlot.setWidth(plotWidth + "px");
		valuePlot.setHeight(plotHeight + "px");	
		
		drawGrid();
				
		if (valuePlot == null) {
			mainPanel.add(new Label(upgradeMessage));
		}
		else 
		{
			mainPanel.add(valuePlot, 20, 360);
		}
	}

	public EnergyMeterUI(String str) {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setID(String id)
	{
		idLabel.setText(id);
	}
	
	public void setDateTime(String dateTime)
	{
		dateTimeLabel.setText(dateTime);
	}
	
	public void setActivePower(String activePower)
	{
		activePowerLabel.setText(activePower + " W");
		
		drawUpdate(Double.parseDouble(activePower.replace(",", ".")));
	}
	
	public void setApparentPower(String apparentPower)
	{
		apparentPowerLabel.setText(apparentPower + " VA");
	}
	
	public void setReactivePower(String reactivePower)
	{
		reactivePowerLabel.setText(reactivePower + " var");
	}
	
	public void setVoltage(String voltage)
	{
		voltageLabel.setText(voltage + " V");
	}
	
	public void setCurrent(String current)
	{
		currentLabel.setText(current + " A");
	}
	
	public void setPowerFactor(String powerFactor)
	{
		powerFactorLabel.setText(powerFactor);
	}
	
	public void setActiveEnergy(String activeEnergy)
	{
		activeEnergyLabel.setText(activeEnergy + " kWh");
	}
	
	public void setApparentEnergy(String apparentEnergy)
	{
		apparentEnergyLabel.setText(apparentEnergy + " kVAh");
	}
	
	public void setReactiveEnergy(String reactiveEnergy)
	{
		reactiveEnergyLabel.setText(reactiveEnergy + " kvarh");
	}
	
	public void setLoadType(String load)
	{
		loadTypeLabel.setText(load);
	}
	
	private void drawGrid()
	{
		Context2d context = valuePlot.getContext2d();
		
		context.setFillStyle("white");
		context.fillRect(0, 0, plotWidth, plotHeight);
		
		context.save();
		context.beginPath();
        context.setStrokeStyle("black");
        context.setLineWidth(2.0);
        context.moveTo(1, 0);
        context.lineTo(1, plotHeight);
        context.lineTo(plotWidth, plotHeight);
        context.stroke();
        context.restore();
		
        context.save();
		context.beginPath();
        context.setStrokeStyle("black");
        context.setLineWidth(1.0);
        
        context.lineTo(1, 1);
        context.lineTo(6, 1);
        
        context.setFont("16px sans-serif");
        context.strokeText(maxValue + " W", 5, 17);
        
        int distance = 60;
        		        
        for (int i = plotWidth - distance; i >= 0; i -= distance)
			{
        		context.moveTo(i, plotHeight - 1);
        		context.lineTo(i, plotHeight - 5);
			}
        
        context.stroke();
        context.restore();
	}
	
	private void drawUpdate(double newValue)
	{
		int result = (int) Math.round(newValue);
		
		values.add(result);
		
		maxValue = 10 * (Collections.max(values) / 10 + 1);
		
		if (values.size() > plotWidth)
		{
			values.remove();
		}
		
		Context2d context = valuePlot.getContext2d();
		context.clearRect(0, 0, plotWidth, plotHeight);	
		
		drawGrid();

		double scale = maxValue - minValue;
		int x = plotWidth - 2;
		double scaleFactor = plotHeight / scale;
		
		context.save();
		context.setStrokeStyle("red");
        context.setLineWidth(2.0);
        context.beginPath();
        context.moveTo(x, ((maxValue - values.getLast()) * scaleFactor - 1));
        		        				
		for (int i = values.size() - 2; i >= 1; i--)
		{
			x -= REFRESH_RATE;
			context.lineTo(x, (maxValue - values.get(i)) * scaleFactor - 1);
		}
		
        context.stroke();
        context.restore();
	}
	
}
