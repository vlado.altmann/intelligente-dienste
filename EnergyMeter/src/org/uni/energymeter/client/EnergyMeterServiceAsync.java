package org.uni.energymeter.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface EnergyMeterServiceAsync {

	void getData(int deviceNumber, AsyncCallback<String[]> callback);

	void getDevices(AsyncCallback<Integer> callback);

}
