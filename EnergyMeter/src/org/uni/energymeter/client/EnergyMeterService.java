package org.uni.energymeter.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("dataservice")
public interface EnergyMeterService extends RemoteService{
	String[] getData(int deviceNumber);
	int getDevices();
}
