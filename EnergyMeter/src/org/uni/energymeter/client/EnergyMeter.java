package org.uni.energymeter.client;

//import org.uni.energymeter.shared.FieldVerifier;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EnergyMeter implements EntryPoint {
	
	private EnergyMeterServiceAsync smartMeterService = GWT.create(EnergyMeterService.class);
	
	private static final int REFRESH_INTERVAL = 1000; //ms
	private int number = 0;
	private HorizontalPanel mainPanel;
	private HorizontalPanel searchingPanel;
	private Label searchingLabel;
	private Image image;
	private Timer refreshTimer;
	
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();
		
		mainPanel = new HorizontalPanel();
		mainPanel.setSpacing(10);
		rootPanel.add(mainPanel);
		
		searchingPanel = new HorizontalPanel();
		searchingPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		rootPanel.add(searchingPanel, 85, 182);
		
		image = new Image("img/progress.gif");
		searchingPanel.add(image);
		
		searchingLabel = new Label("    Searching for Devices...");
		searchingPanel.add(searchingLabel);
		searchingLabel.setSize("191px", "38px");
		searchingLabel.setStyleName("heading");
		
		refreshTimer = new Timer() {
			@Override
			public void run() {
				refreshValues();
			}
		};
		
//		Timer searchingTimer = new Timer(){
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
////				searchingPanel.setVisible(false);
//				image.setVisible(false);
//			}
//			
//		};
//		
//		searchingTimer.schedule(5000);
		
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {
			public void onFailure(Throwable caught) {
				// TODO: Do something with errors.
				image.setVisible(false);
			}

			@Override
			public void onSuccess(Integer result) {
				
				image.setVisible(false);
				
				number = result;
				
				for (int i = 0; i < result; i++)
				{
					EnergyMeterUI ui = new EnergyMeterUI();
					mainPanel.add(ui);										
				}
				
				if (result > 0)
				{
					searchingLabel.setVisible(false);
					refreshTimer.scheduleRepeating(REFRESH_INTERVAL);
				}
				else
				{
					searchingLabel.setText("No Devices were found");
					searchingLabel.setVisible(true);
				}
				
				refreshValues();
				System.out.println("devices found: " + result);
			}
		};

		smartMeterService.getDevices(callback);
//		ui.setDateTime("");
	}
	
	private void refreshValues()
	{
//		System.out.println("requesting new values");
		AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String[] result) {
				EnergyMeterUI ui = (EnergyMeterUI) mainPanel.getWidget(Integer.parseInt(result[0]));
				ui.setID(result[1]);
				ui.setDateTime(result[2]);
				ui.setActivePower(result[3]);
				ui.setApparentPower(result[4]);
				ui.setReactivePower(result[5]);
				ui.setVoltage(result[6]);
				ui.setCurrent(result[7]);
				ui.setPowerFactor(result[8]);
				ui.setActiveEnergy(result[9]);
				ui.setApparentEnergy(result[10]);
				ui.setReactiveEnergy(result[11]);
				ui.setLoadType(result[12]);
			}
		};
		
		for (int i = 0; i < number; i++)
		{
			smartMeterService.getData(i, callback);
		}
	}
}
