package org.uni.energymeter.server;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.ws4d.java.client.DefaultClient;
import org.ws4d.java.client.SearchManager;
import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.dispatch.DeviceServiceRegistry;
import org.ws4d.java.schema.ComplexType;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.security.SecurityKey;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.service.parameter.StringValue;
import org.ws4d.java.types.SearchParameter;

public class DPWSClient extends DefaultClient  {
	
	static final String ENERGY_METER = "EnergyMeter";
	static final String ENERGY_METER_SERVICE = "EnergyMeterService";
	static final String TYPE_ENERGY_METER = "EnergyMeterInterface";
	static final String NAMESPACE_ENERGY_METER = "http://www.ws4d.org/SmartMeter/EnergyMeter";
	
	private List<Device> deviceList;
	private List<Service> serviceList;
	
	public DPWSClient()
	{
		deviceList = new LinkedList<Device>();
	}
	
	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search){
		Device device = null;
		try {
			device = devRef.getDevice();

			for (Iterator i = device.getPortTypes(); i.hasNext();)
			{
				QName name = (QName) i.next();
				if (name.getLocalPart().equals(ENERGY_METER))
				{
					if (!deviceList.contains(device))
					{
						EndpointReference endRef = devRef.getEndpointReference();
						System.out.println("Found Device: " + device.getFriendlyName("en"));
						System.out.println(endRef);
						System.out.println("Port type:" + name.getLocalPart());
						System.out.println("UUID:" + devRef.getEndpointReference().getAddress());
						deviceList.add(device);

						if (serviceList == null){
							serviceList = new LinkedList<Service>();
						}

						for (Iterator it_servRef = device.getServiceReferences(SecurityKey.EMPTY_KEY); it_servRef.hasNext();) {
							ServiceReference servRef = (ServiceReference) it_servRef.next();
							Service service = null;

							service = servRef.getService();


							String id = service.getServiceId().toString();
							System.out.println("Service: " + id);
							serviceList.add(service);
							if (id.equals(DPWSClient.ENERGY_METER_SERVICE)) {
								System.out.println("Found Energy Meter Service");
							} else {
								System.out.println("Found Generic Service");
							}
						}

						break;
					}
				}
			}
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Devices: " + deviceList.size());
		//		deviceList.add(device);
	}
	
	static void displayInterfaceInformation(NetworkInterface netint) throws SocketException {
        System.out.printf("Display name: %s\n", netint.getDisplayName());
        System.out.printf("Name: %s\n", netint.getName());
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
        	System.out.printf("InetAddress: %s\n", inetAddress);
        }
        System.out.printf("\n");
     }
	
	public void getStaticDevice()
	{				
//		try {
//			InetAddress thisIp = InetAddress.getLocalHost();
//			System.out.println("IP: " + thisIp.getHostAddress());
//
//			NetworkInterface inet = NetworkInterface.getByInetAddress(thisIp);
//			displayInterfaceInformation(inet);
//
//			DeviceReference devRef = SearchManager.getDeviceReference(new EndpointReference(new URI("urn:uuid:5afda9ab-45d6-41e1-9d64-3e3cdf127836")),
//					new URI("http://139.30.201.249:59876/5afda9ab-45d6-41e1-9d64-3e3cdf127836"), null, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
//
//			Device device = devRef.getDevice();
//			deviceList.add(device);
//
//			System.out.println("Found Device: " + device.getFriendlyName("en"));
////			devicePane.addDevice(device);
//
//		} catch (UnknownHostException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
//		catch (SocketException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		catch (TimeoutException e) {
//			e.printStackTrace();
//		}		
	}
	
	public void getDeviceList(){	
		SearchParameter search = new SearchParameter();
		QNameSet qnameSet = new QNameSet();
		qnameSet.add(new QName(ENERGY_METER, NAMESPACE_ENERGY_METER));
		search.setDeviceTypes(qnameSet);
		SearchManager.searchDevice(search, this, null);
	}
	
	public List<Device> getList()
	{
		return deviceList;
	}
	
	public int getDeviceCount()
	{
		return deviceList.size();
	}
	
	public String[] getActualValues(int deviceNumber)
	{
		if (deviceNumber > serviceList.size() - 1) 
			return null;
		
		Service service = serviceList.get(deviceNumber);
		
		Operation op = service.getOperation(null, "GetData", null, null);
		
		ParameterValue input = op.createInputValue();
		ParameterValue returnMessagePV;
		String[] returnValue = new String[13];
		
		// Workaround for ReactiveEnergy bug
		for (int i = 0; i < 13; i++)
		{
			returnValue[i] = "n/a";
		}
		
		try {
			// We invoke our TwoWay operation. The answer will be returned by
			// the invoke method.
			returnMessagePV = op.invoke(input, CredentialInfo.EMPTY_CREDENTIAL_INFO);
//			System.out.println(returnMessagePV.toString());
			returnValue[0] = "" + deviceNumber;
			returnValue[1] = deviceList.get(deviceNumber).getSerialNumber();
			returnValue[2] = ParameterValueManagement.getString(returnMessagePV, "DateTime");
			returnValue[3] = ParameterValueManagement.getString(returnMessagePV, "ActivePower");
			returnValue[4] = ParameterValueManagement.getString(returnMessagePV, "ApparentPower");
			returnValue[5] = ParameterValueManagement.getString(returnMessagePV, "ReactivePower");
			returnValue[6] = ParameterValueManagement.getString(returnMessagePV, "Voltage");
			returnValue[7] = ParameterValueManagement.getString(returnMessagePV, "Current");
			returnValue[8] = ParameterValueManagement.getString(returnMessagePV, "PowerFactor");
			returnValue[9] = ParameterValueManagement.getString(returnMessagePV, "ActiveEnergy");
			returnValue[10] = ParameterValueManagement.getString(returnMessagePV, "ApparentEnergy");
			returnValue[11] = ParameterValueManagement.getString(returnMessagePV, "ReactiveEnergy");
			returnValue[12] = ParameterValueManagement.getString(returnMessagePV, "Load");
//			returnValue = stateDescription.toString();
//			System.out.println("Smart Meter returns: " + returnValue);
		} catch (InvocationException e) {
			e.printStackTrace();
		} catch (CommunicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			System.out.println("***********************IndexOutOfBoundsException is happened");
			return returnValue;
		}
		return returnValue;
	}
	
}
