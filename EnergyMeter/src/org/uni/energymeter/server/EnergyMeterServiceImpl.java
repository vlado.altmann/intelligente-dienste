package org.uni.energymeter.server;

import java.io.IOException;

import org.uni.energymeter.client.EnergyMeterService;
import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.communication.DPWSProtocolVersion;
import org.ws4d.java.configuration.DPWSProperties;
import org.ws4d.java.configuration.DispatchingProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.configuration.Property;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.ws4d.java.wsdl.WSDLRepository;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class EnergyMeterServiceImpl extends RemoteServiceServlet implements EnergyMeterService{

	private String responceString = new String("Default");
	private DPWSClient dpwsClient;
	
	public EnergyMeterServiceImpl() {

	}
	
	@Override
	public void init()
	{
		// mandatory: Starting the DPWS Framework.
//		JMEDSFramework.start(null);

//		dpwsClient = new DPWSClient();

		//		dpwsClient = new DPWSClient();
		//		responceString = new String("Constructor");
		//		responceString = new String("Init");
		//		dpwsClient = new DPWSClient();
		//		dpwsClient.getDeviceList();
		//		dpwsClient.getStaticDevice();
	}
	
	@Override
	public String[] getData(int deviceNumber) {
//		String responce = dpwsClient.getActualValues(deviceNumber);
//		String[] returnValue = new String[10];
//		returnValue[3] = responce;
//		System.out.println("dev value:" + responce);
		return dpwsClient.getActualValues(deviceNumber);
	}

	@Override
	public int getDevices() {

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		DPWSProperties properties = DPWSProperties.getInstance();
		properties.addSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2006);
//		properties.removeSupportedDPWSVersion(DPWSProtocolVersion.DPWS_VERSION_2009);

//		// mandatory: Starting the DPWS Framework.
		JMEDSFramework.start(null);
//		
		dpwsClient = new DPWSClient();
		dpwsClient.getDeviceList();
		try {
			int i = 0;
			while (dpwsClient.getList().size() <= 0 && i < 10)
			{
				Thread.sleep(1000);
				i++;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Service invoked");
		return dpwsClient.getDeviceCount();
	}

}
